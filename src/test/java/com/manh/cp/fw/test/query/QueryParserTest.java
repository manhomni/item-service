package com.manh.cp.fw.test.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.testng.annotations.Test;

import com.manh.cp.fw.query.expressiontree.Condition;
import com.manh.cp.fw.query.expressiontree.Expression;
import com.manh.cp.fw.query.expressiontree.ExpressionNode;
import com.manh.cp.fw.query.expressiontree.LogicalOperator;
import com.manh.cp.fw.query.parser.ParseException;
import com.manh.cp.fw.query.parser.QueryParser;
import com.manh.cp.fw.query.parser.QueryParserImpl;

@Test(groups="unit")
public class QueryParserTest
{

    @Test
    public void testBasicQuery()
    {
        String query = "orderType = Regular";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(Condition.class, rootNode.getClass());

            Condition condition = (Condition) rootNode;

            assertEquals("orderType", condition.getField());
            assertEquals("=", condition.getOperator());
            assertEquals("Regular", condition.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testBasicQueryWithObject()
    {
        String query = "Order.orderType = Regular";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(Condition.class, rootNode.getClass());

            Condition condition = (Condition) rootNode;

            assertEquals("Order.orderType", condition.getField());
            assertEquals("=", condition.getOperator());
            assertEquals("Regular", condition.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testBasicQueryWithQuote()
    {
        String query = "Order.orderType = 'Regular Order'";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(Condition.class, rootNode.getClass());

            Condition condition = (Condition) rootNode;

            assertEquals("Order.orderType", condition.getField());
            assertEquals("=", condition.getOperator());
            assertEquals("Regular Order", condition.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testBasicQueryWithEscapedQuote()
    {
        String query = "Order.orderType = 'Macy\\'s Order'";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(Condition.class, rootNode.getClass());

            Condition condition = (Condition) rootNode;

            assertEquals("Order.orderType", condition.getField());
            assertEquals("=", condition.getOperator());
            assertEquals("Macy's Order", condition.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testLogicalOperatorAND()
    {
        String query = "Order.orderType = Regular AND Order.confirmed = true";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(LogicalOperator.class, rootNode.getClass());

            LogicalOperator and = (LogicalOperator) rootNode;

            assertEquals("AND", and.getLogicalOperator());
            assertEquals(2, and.getChildNodes().size());

            Condition condition1 = (Condition) and.getChildNodes().get(0);
            assertEquals("Order.orderType", condition1.getField());
            assertEquals("=", condition1.getOperator());
            assertEquals("Regular", condition1.getValue());

            Condition condition2 = (Condition) and.getChildNodes().get(1);
            assertEquals("Order.confirmed", condition2.getField());
            assertEquals("=", condition2.getOperator());
            assertEquals("true", condition2.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testLogicalOperatorOR()
    {
        String query = "Order.orderType = Regular OR Order.confirmed = true";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(LogicalOperator.class, rootNode.getClass());

            LogicalOperator or = (LogicalOperator) rootNode;

            assertEquals("OR", or.getLogicalOperator());
            assertEquals(2, or.getChildNodes().size());

            Condition condition1 = (Condition) or.getChildNodes().get(0);
            assertEquals("Order.orderType", condition1.getField());
            assertEquals("=", condition1.getOperator());
            assertEquals("Regular", condition1.getValue());

            Condition condition2 = (Condition) or.getChildNodes().get(1);
            assertEquals("Order.confirmed", condition2.getField());
            assertEquals("=", condition2.getOperator());
            assertEquals("true", condition2.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testLogicalOperatorANDWithBraces()
    {
        String query = "(Order.orderType = Regular AND Order.confirmed = true)";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(LogicalOperator.class, rootNode.getClass());

            LogicalOperator and = (LogicalOperator) rootNode;

            assertEquals("AND", and.getLogicalOperator());
            assertEquals(2, and.getChildNodes().size());

            Condition condition1 = (Condition) and.getChildNodes().get(0);
            assertEquals("Order.orderType", condition1.getField());
            assertEquals("=", condition1.getOperator());
            assertEquals("Regular", condition1.getValue());

            Condition condition2 = (Condition) and.getChildNodes().get(1);
            assertEquals("Order.confirmed", condition2.getField());
            assertEquals("=", condition2.getOperator());
            assertEquals("true", condition2.getValue());

        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testComplexQuery()
    {
        String query = "((Order.orderType = Regular AND Order.confirmed = true) "
                + "OR (Order.orderType = 'Macy\\'s Order' AND Order.confirmed = false))";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(LogicalOperator.class, rootNode.getClass());

            LogicalOperator or = (LogicalOperator) rootNode;

            assertEquals("OR", or.getLogicalOperator());
            assertEquals(2, or.getChildNodes().size());

            LogicalOperator and1 = (LogicalOperator) or.getChildNodes().get(0);

            assertEquals("AND", and1.getLogicalOperator());
            assertEquals(2, and1.getChildNodes().size());

            Condition condition11 = (Condition) and1.getChildNodes().get(0);
            assertEquals("Order.orderType", condition11.getField());
            assertEquals("=", condition11.getOperator());
            assertEquals("Regular", condition11.getValue());

            Condition condition12 = (Condition) and1.getChildNodes().get(1);
            assertEquals("Order.confirmed", condition12.getField());
            assertEquals("=", condition12.getOperator());
            assertEquals("true", condition12.getValue());

            LogicalOperator and2 = (LogicalOperator) or.getChildNodes().get(1);

            assertEquals("AND", and2.getLogicalOperator());
            assertEquals(2, and2.getChildNodes().size());

            Condition condition21 = (Condition) and2.getChildNodes().get(0);
            assertEquals("Order.orderType", condition21.getField());
            assertEquals("=", condition21.getOperator());
            assertEquals("Macy's Order", condition21.getValue());

            Condition condition22 = (Condition) and2.getChildNodes().get(1);
            assertEquals("Order.confirmed", condition22.getField());
            assertEquals("=", condition22.getOperator());
            assertEquals("false", condition22.getValue());
        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testIncompleteQuery()
    {
        String query = "orderType = ";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            parser.parse();
        }
        catch (ParseException e)
        {
            assertEquals(true, true);
        }
    }
}
