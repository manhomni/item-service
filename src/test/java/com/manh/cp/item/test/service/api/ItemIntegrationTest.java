/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.item.test.service.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.manh.cp.ItemServiceApplication;
import com.manh.cp.item.api.domain.ItemBase;

@SpringApplicationConfiguration(classes = ItemServiceApplication.class)
@WebIntegrationTest("server.port:0")
@DirtiesContext
@Test(groups = "integration", dependsOnGroups = "service")
public class ItemIntegrationTest extends AbstractTestNGSpringContextTests
{

	@Value("${local.server.port}")
    private int port;

    private final RestTemplate restTemplate = new TestRestTemplate("user", "password");

    private String addedPK;

    private final String organizationId = "ORG-101";

    private final String itemId = "ITEM-10001";

    private final String description = "Test Description";

    private String getBaseURL()
    {
        return "http://localhost:" + port;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void testCreateItem()
    {

        Map<String, Object> entityMap = new HashMap<String, Object>();

        entityMap.put(ItemBase.Fields.ORGANIZATION_ID.getCode(), organizationId);
        entityMap.put(ItemBase.Fields.ITEM_ID.getCode(), itemId);

        HttpEntity<Map<String, Object>> httpEntity = new HttpEntity<Map<String, Object>>(entityMap);

        ResponseEntity<Map> response = restTemplate.exchange(
                getBaseURL() + "/api/itemservice/item", HttpMethod.POST, httpEntity, Map.class);

        Map<String, Object> outputMap = response.getBody();
        addedPK = (String) outputMap.get(ItemBase.Fields.PK.getCode());
        Assert.assertNotNull(addedPK);

    }

    @SuppressWarnings({ "rawtypes" })
    @Test(dependsOnMethods = "testCreateItem")
    public void testGetItem()
    {
        Map outputMap = restTemplate.getForObject(
                getBaseURL() + "/api/itemservice/item/" + addedPK, Map.class);

        Assert.assertEquals(outputMap.get(ItemBase.Fields.PK.getCode()), addedPK);
        Assert.assertEquals(outputMap.get(ItemBase.Fields.ORGANIZATION_ID.getCode()),
                organizationId);
        Assert.assertEquals(outputMap.get(ItemBase.Fields.ITEM_ID.getCode()), itemId);
    }

    @SuppressWarnings({ "unchecked" })
    @Test(dependsOnMethods = "testCreateItem")
    public void testUpdateItem()
    {

        Map<String, Object> entityMap = new HashMap<String, Object>();

        entityMap.put(ItemBase.Fields.ORGANIZATION_ID.getCode(), organizationId);
        entityMap.put(ItemBase.Fields.ITEM_ID.getCode(), itemId);
        entityMap.put(ItemBase.Fields.DESCRIPTION.getCode(), description);

        restTemplate.put(getBaseURL() + "/api/itemservice/item/" + addedPK, entityMap);

        Map<String, Object> outputMap = restTemplate.getForObject(getBaseURL()
                + "/api/itemservice/item/" + addedPK, Map.class);
        Assert.assertEquals(outputMap.get(ItemBase.Fields.DESCRIPTION.getCode()), description);

    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test(dependsOnMethods = "testCreateItem")
    public void testSearchItemById()
    {
        List outputList = restTemplate.getForObject(getBaseURL()
                + "/api/itemservice/item/search?query=\"ItemId = '" + itemId + "'\"", List.class);
        Assert.assertEquals(outputList.size(), 1);
        Map<String, Object> output = (Map<String, Object>) outputList.get(0);
        Assert.assertEquals(output.get(ItemBase.Fields.ITEM_ID.getCode()), itemId);
        Assert.assertEquals(output.get(ItemBase.Fields.ORGANIZATION_ID.getCode()),
                organizationId);
    }

}
