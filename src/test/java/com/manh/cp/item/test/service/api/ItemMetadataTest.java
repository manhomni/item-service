/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.test.service.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.manh.cp.ItemServiceApplication;
import com.manh.cp.fw.entity.domain.entity.CPEntityBase;
import com.manh.cp.fw.entity.domain.metadata.CPBasicFieldMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPFieldMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPOneToManyFieldMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPTemplate;
import com.manh.cp.item.api.domain.Item;
import com.manh.cp.item.api.domain.ItemMedia;
import com.manh.cp.item.api.domain.ItemSubstitution;
import com.manh.cp.item.api.domain.ItemSubstitutionDetail;

@SpringApplicationConfiguration(classes = ItemServiceApplication.class)
@WebAppConfiguration
@Test(groups="unit")
public class ItemMetadataTest extends AbstractTestNGSpringContextTests
{
    @DataProvider(name = "EntityNameProvider")
    Iterator<Object[]> getEntityName()
    {
        List<Object[]> result = new ArrayList<>();
        result.add(new Object[] { Item.ENTITY_NAME, Item.Fields.getCodes() });
        result.add(new Object[] { ItemMedia.ENTITY_NAME, ItemMedia.Fields.getCodes() });
        result.add(new Object[] { ItemSubstitution.ENTITY_NAME, ItemSubstitution.Fields.getCodes() });
        result.add(new Object[] { ItemSubstitutionDetail.ENTITY_NAME, ItemSubstitutionDetail.Fields.getCodes() });

        return result.iterator();

    }

    @Test(dataProvider = "EntityNameProvider")
    public void testReadMetadata(String entityName, List<String> codes) throws Exception
    {
        CPMetadata metadata = CPEntityBase.getMetadata(entityName);
        Assert.assertNotNull(metadata);
        Assert.assertEquals(metadata.getEntityName(), entityName);

        codes.forEach((jsonCode)->{
            CPFieldMetadata fieldMetadata = metadata.getField(jsonCode);
            Assert.assertNotNull(fieldMetadata);

            switch (fieldMetadata.getAttributeType())
            {
                case BASIC:
                    CPBasicFieldMetadata basicFieldMetadata = (CPBasicFieldMetadata)fieldMetadata;
                    Assert.assertNotNull(basicFieldMetadata.getFieldName());
                    break;
                case ONE_TO_MANY:
                    CPOneToManyFieldMetadata oneToManyFieldMetadata = (CPOneToManyFieldMetadata) fieldMetadata;
                    Assert.assertNotNull(oneToManyFieldMetadata.getTargetEntityName());
                    break;
                default:
                    Assert.fail("Unexpected attribute type in metadata");
            }
        });
    }

    @Test
    public void testTemplates()
    {
        CPMetadata metadata = CPEntityBase.getMetadata("Item");
        Assert.assertNotNull(metadata);

        CPTemplate template = metadata.getDefaultTemplate();
        Assert.assertNotNull(template);
        Assert.assertEquals(template.getEntityName(), "Item");

        testTemplate(template, Item.Fields.getCodes(), metadata);
    }

    private void testTemplate(CPTemplate template, List<String> codes, CPMetadata metadata)
    {
        codes.forEach((jsonCode)->{

            CPFieldMetadata fieldMetadata = metadata.getField(jsonCode);

            switch (fieldMetadata.getAttributeType())
            {
                case BASIC:
                    Assert.assertTrue(template.isFieldSet(jsonCode));
                    break;
                case ONE_TO_MANY:
                    Assert.assertTrue(template.isChildSet(jsonCode));
                    CPMetadata childMetadata = CPEntityBase.getMetadata(((CPOneToManyFieldMetadata)fieldMetadata).getTargetEntityName());
                    testTemplate(template.getChild(jsonCode), childMetadata.getCodes(), childMetadata);
                    break;
                default:
                    Assert.fail("Unexpected attribute type in metadata");
            }
        });
    }

}
