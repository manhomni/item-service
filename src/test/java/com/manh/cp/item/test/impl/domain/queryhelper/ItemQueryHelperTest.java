/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.item.test.impl.domain.queryhelper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.testng.annotations.Test;

import com.manh.cp.fw.entity.CPContext;
import com.manh.cp.fw.query.expressiontree.Condition;
import com.manh.cp.fw.query.expressiontree.Expression;
import com.manh.cp.fw.query.expressiontree.ExpressionNode;
import com.manh.cp.fw.query.parser.ParseException;
import com.manh.cp.fw.query.parser.QueryParser;
import com.manh.cp.fw.query.parser.QueryParserImpl;
import com.manh.cp.item.impl.domain.queryhelper.ItemQueryHelper;
import com.mysema.query.types.Predicate;

/**
 * @author RaKrishnan
 *
 */
@Test(groups = "unit")
public class ItemQueryHelperTest
{

    private CPContext context = new CPContext("test");
    
    //TODO Autowire
    private ItemQueryHelper queryHelper = new ItemQueryHelper();

    @Test
    public void testBasicQuery()
    {
        
        String query = "ItemId = 'ITEM-10001'";
        QueryParser parser = new QueryParserImpl(query);
        try
        {
            Expression expression = parser.parse();
            ExpressionNode rootNode = expression.getRoot();

            assertNotNull(rootNode);
            assertEquals(Condition.class, rootNode.getClass());

            Condition condition = (Condition) rootNode;

            assertEquals("ItemId", condition.getField());
            assertEquals("=", condition.getOperator());
            assertEquals("ITEM-10001", condition.getValue());
            
            Predicate predicate = queryHelper.buildQuery(context, expression);
            System.out.println(predicate);
        }
        catch (ParseException e)
        {
            fail(e.getMessage());
        }
        
    }

}
