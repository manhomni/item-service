/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.test.service.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.manh.cp.ItemServiceApplication;
import com.manh.cp.fw.entity.CPContext;
import com.manh.cp.fw.entity.domain.embeddable.Dimension;
import com.manh.cp.fw.entity.domain.embeddable.Quantity;
import com.manh.cp.fw.entity.domain.entity.CPEntityBase;
import com.manh.cp.fw.entity.domain.enums.ActiveStatus;
import com.manh.cp.fw.entity.domain.enums.UOM;
import com.manh.cp.fw.entity.support.factory.EntityFactory;
import com.manh.cp.item.api.domain.Item;
import com.manh.cp.item.api.domain.ItemBase;
import com.manh.cp.item.api.domain.ItemMedia;
import com.manh.cp.item.api.domain.ItemMediaBase;
import com.manh.cp.item.api.domain.ItemSubstitution;
import com.manh.cp.item.api.domain.ItemSubstitutionBase;
import com.manh.cp.item.api.domain.ItemSubstitutionDetail;
import com.manh.cp.item.api.domain.ItemSubstitutionDetailBase;
import com.manh.cp.item.api.service.ItemService;
import com.manh.cp.item.impl.domain.entity.ItemEntity;
import com.manh.cp.item.impl.domain.enums.ItemStatus;
import com.manh.cp.item.impl.domain.enums.ItemSubstitutionType;
import com.manh.cp.item.impl.domain.enums.MediaSize;
import com.manh.cp.item.impl.domain.enums.PriceStatus;

@SpringApplicationConfiguration(classes = ItemServiceApplication.class)
@WebAppConfiguration
@Test(groups="service", dependsOnGroups = "unit")
public class ItemServiceTest extends AbstractTransactionalTestNGSpringContextTests
{
    @Resource
    protected ItemService service;

    private Long addedPk;

    private final CPContext context = new CPContext("MyUser");

    @PersistenceContext
    protected EntityManager entityManager;

    @Test
    @Rollback(false)
    public void testCreateItem() throws Exception
    {
        assertInTransaction();

        Item savedItem = createItem(true);

        // Re-fetch it to pull the DB changes
        savedItem = service.findItemByPK(context, savedItem.getPk());
        Assert.assertNotNull(savedItem);
//        Assert.assertEquals(item.getPk(), savedItem.getPk());
        checkFullItem(savedItem, "MyBrand");

        this.addedPk = savedItem.getPk();
    }

    @Test
    @Rollback(true)
    public void testCreateItemUsingProperties() throws Exception
    {
        assertInTransaction();

        Item savedItem = createItem(false);

        // Re-fetch it to pull the DB changes
        savedItem = service.findItemByPK(context, savedItem.getPk());
        Assert.assertNotNull(savedItem);
        //        Assert.assertEquals(item.getPk(), savedItem.getPk());
        checkFullItemUsingProperties(savedItem, "MyBrand");

    }


    private Item createItem(boolean useMethods) throws Exception
    {
        Item item = EntityFactory.getFactory().newEntity(entityManager, ItemEntity.class, "MyOrg", "MyItem");
        if (useMethods)
        {
            setAllFieldsNonDefault(item);
        }
        else
        {
            setAllFieldsUsingPropertyMap(item);
        }
        Assert.assertNotNull(item.getPk());

        Item savedItem = service.add(context, item);

        return savedItem;
    }

    @Test(dependsOnMethods = "testCreateItem")
    public void testFindById()
    {
        assertInTransaction();

        Item item = service.findItemByPK(context, addedPk);

        Assert.assertNotNull(item);
        Assert.assertEquals(item.getOrganizationId(), "MyOrg");
        Assert.assertEquals(item.getItemId(), "MyItem");
        Assert.assertFalse(((CPEntityBase)item).isNew());

        checkFullItem(item, "MyBrand");
//        Assert.assertTrue(item.getCreatedTimestamp().isBefore(item.getUpdatedTimestamp()));
    }

    @DataProvider(name="UpdateDataProvider")
    public Iterator<Object[]> getUpdateDataProvider()
    {
        List<Object[]> result = new ArrayList<>();
        result.add(new Object[] {"MyBrand2"});
        result.add(new Object[] { "MyBrand3" });
        result.add(new Object[] { "MyBrand4" });

        return result.iterator();
    }

    @Test(enabled = true, dependsOnMethods = "testCreateItem", dataProvider = "UpdateDataProvider")
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    @Rollback(false)
    public void testSimpleUpdate(String newBrand) throws Exception
    {
        assertNotInTransaction();

        Item item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        item.setBrand(newBrand);

        // The following should be on a single transaction with commit
        Item updatedItem = service.update(context, item);
        Assert.assertNotNull(updatedItem);
        Assert.assertEquals(updatedItem.getBrand(), newBrand);

        checkFullItem(updatedItem, newBrand);

        // And now another transaction
        updatedItem.setBrand("MyBrand");
        updatedItem = service.update(context, updatedItem);

        updatedItem = service.findItemByPK(context, updatedItem.getPk());
        checkFullItem(updatedItem, "MyBrand");

        // The created timestamp should be earlier than the updated
//        Assert.assertTrue(updatedItem.getCreatedTimestamp().compareTo(updatedItem.getUpdatedTimestamp()) < 0);

    }

    @Test (dependsOnMethods = {"testCreateItem", "testSimpleUpdate"})
    public void testUpdateWithoutExplicitSave()
    {
        // The Spring test framework starts a transaction for us at the beginning of the test method
        assertInTransaction();

        Item item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        // Now we modify something on this instance, still within the same transaction
        item.setColorSequence(7);

        item = service.findItemByPK(context, addedPk);
        Assert.assertEquals(item.getColorSequence(), (Integer) 7);

        // Now we end the transaction within the test, committing the transaction without an explicit save.
        TestTransaction.flagForCommit();
        TestTransaction.end();
        assertNotInTransaction();

        // Query again to get the item. This will start a new transaction within the service API.
        item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        // See that the change was persisted.
        Assert.assertEquals(item.getColorSequence(), new Integer(7));
    }

    @Test (dependsOnMethods = "testCreateItem")
    public void testCachingWithinTransaction()
    {
        assertInTransaction();

        Item item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        // Now we modify something on this instance
        item.setColorSequence(5);

        // Query again to get the item
        item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        Assert.assertEquals(item.getColorSequence(), new Integer(5));

    }

    @Test (dependsOnMethods = "testCreateItem")
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void testCachingOutsideTransaction()
    {
        Item item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        // Now we modify something on this instance
        item.setColorSequence(5);

        // Query again to get the item
        item = service.findItemByPK(context, addedPk);
        Assert.assertNotNull(item);

        Assert.assertEquals(item.getColorSequence(), new Integer(3));
    }

    private void setAllFieldsNonDefault(Item item) throws Exception
    {
        item.setDescription("MyDesc");
        item.setLongDescription("MyLongDesc");
        item.setShortDescription("MyShortDesc");
        item.setBaseUOM(UOM.UNITS);
        item.setSeason("Fall");
        item.setStyle("MyStyle");
        item.setColorCode("Red");
        item.setSize("XXL");
        item.setUpc("abcd");
        item.setEan("xyz");
        item.setGttn("gttn");
        item.setWeight(new Quantity(25, UOM.UNITS));
        item.setVolume(new Quantity(5, UOM.GALLONS));
        item.setDimension(new Dimension(3, 4, 5, UOM.UNITS));
        item.setCatchWeightFlag(true);
        item.setStatus(ItemStatus.STATUS_THREE);
        item.setItemLargeImageUri("largeURI");
        item.setItemSmallImageUri("smallURI");
        item.setColorDescription("RedDesc");
        item.setSoldOnline(false);
        item.setSoldInStores(false);
        item.setPickupAtStore(false);
        item.setShipToStore(false);
        item.setIsReturnable(false);
        item.setIsExchangeable(false);
        item.setIsSubstitutable(false);
        item.setWebsiteUrl("websiteUrl");
        item.setPriceStatus(PriceStatus.STATUS_THREE);
        item.setSellThrough(256.78);
        item.setBrand("MyBrand");
        item.setSizeSequence(4);
        item.setColorSequence(3);

        for (int i = 0; i < 3; i++)
        {
            String uriId = "URI_ID_" + (i + 1);
            ItemMedia itemMedia = item.createItemMedia(entityManager, uriId);
            itemMedia.setUri("URI_" + (i + 1));
            itemMedia.setMimeType("image/bmp");
            itemMedia.setMediaSize(MediaSize.LARGE);
            itemMedia.setExtended("testCol", "ABC");
        }

        for (int i = 0; i < 3; i++)
        {
            String ruleId = "RULE_ID_" + (i + 1);
            ItemSubstitution itemSubstitution = item.createItemSubstitution(entityManager, ruleId);
            itemSubstitution.setSubstitutionType(ItemSubstitutionType.IN_ADDITION);
            itemSubstitution.setSubstitutionStatus(ActiveStatus.INACTIVE);
            itemSubstitution.setStartDate(LocalDate.now());
            itemSubstitution.setEndDate(LocalDate.now());
            itemSubstitution.setBaseItemQty(3.5);
            itemSubstitution.setDescription("MyDescr");

            for (int j = 0; j < 1; j++)
            {
                ItemSubstitutionDetail itemSubstitutionDetail = itemSubstitution.createItemSubstitutionDetail(entityManager, item);
                itemSubstitutionDetail.setSubstitutionStatus(ActiveStatus.INACTIVE);
                itemSubstitutionDetail.setRank(j);
                itemSubstitutionDetail.setItemQtyRatio(4.5);
            }
        }
    }

    private void setAllFieldsUsingPropertyMap(Item item) throws Exception
    {
        item.setProperty(ItemBase.Fields.DESCRIPTION.getCode(), "MyDesc");
        item.setProperty(ItemBase.Fields.LONG_DESCRIPTION.getCode(), "MyLongDesc");
        item.setProperty(ItemBase.Fields.SHORT_DESCRIPTION.getCode(), "MyShortDesc");
        item.setProperty(ItemBase.Fields.BASE_UOM.getCode(), UOM.UNITS);
        item.setProperty(ItemBase.Fields.SEASON.getCode(), "Fall");
        item.setProperty(ItemBase.Fields.STYLE.getCode(), "MyStyle");
        item.setProperty(ItemBase.Fields.COLOR_CODE.getCode(), "Red");
        item.setProperty(ItemBase.Fields.SIZE.getCode(), "XXL");
        item.setProperty(ItemBase.Fields.UPC.getCode(), "abcd");
        item.setProperty(ItemBase.Fields.EAN.getCode(), "xyz");
        item.setProperty(ItemBase.Fields.GTTN.getCode(), "gttn");
        item.setProperty(ItemBase.Fields.WEIGHT.getCode(), new Quantity(25, UOM.UNITS));
        item.setProperty(ItemBase.Fields.VOLUME.getCode(), new Quantity(5, UOM.GALLONS));
        item.setProperty(ItemBase.Fields.DIMENSION.getCode(), new Dimension(3, 4, 5, UOM.UNITS));
        item.setProperty(ItemBase.Fields.CATCH_WEIGHT_FLAG.getCode(), true);
        item.setProperty(ItemBase.Fields.STATUS.getCode(), ItemStatus.STATUS_THREE);
        item.setProperty(ItemBase.Fields.LARGE_IMAGE_URI.getCode(), "largeURI");
        item.setProperty(ItemBase.Fields.SMALL_IMAGE_URI.getCode(), "smallURI");
        item.setProperty(ItemBase.Fields.COLOR_DESCRIPTION.getCode(), "RedDesc");
        item.setProperty(ItemBase.Fields.SOLD_ONLINE.getCode(), false);
        item.setProperty(ItemBase.Fields.SOLD_IN_STORES.getCode(), false);
        item.setProperty(ItemBase.Fields.PICKUP_AT_STORE.getCode(), false);
        item.setProperty(ItemBase.Fields.SHIP_TO_STORE.getCode(), false);
        item.setProperty(ItemBase.Fields.IS_RETURNABLE.getCode(), false);
        item.setProperty(ItemBase.Fields.IS_EXCHANGEABLE.getCode(), false);
        item.setProperty(ItemBase.Fields.IS_SUBSTITUTABLE.getCode(), false);
        item.setProperty(ItemBase.Fields.WEBSITE_URL.getCode(), "websiteUrl");
        item.setProperty(ItemBase.Fields.PRICE_STATUS.getCode(), PriceStatus.STATUS_THREE);
        item.setProperty(ItemBase.Fields.SELL_THROUGH.getCode(), 256.78);
        item.setProperty(ItemBase.Fields.BRAND.getCode(), "MyBrand");
        item.setProperty(ItemBase.Fields.SIZE_SEQUENCE.getCode(), 4);
        item.setProperty(ItemBase.Fields.COLOR_SEQUENCE.getCode(), 3);

        for (int i = 0; i < 3; i++)
        {
            String uriId = "URI_ID_" + (i + 1);
            ItemMedia itemMedia = item.createItemMedia(entityManager, uriId);
            itemMedia.setProperty(ItemMediaBase.Fields.URI.getCode(), "URI_" + (i + 1));
            itemMedia.setProperty(ItemMediaBase.Fields.MIME_TYPE.getCode(), "image/bmp");
            itemMedia.setProperty(ItemMediaBase.Fields.MEDIA_SIZE.getCode(), MediaSize.LARGE);
            itemMedia.setProperty("testCol", "XYZ");
        }

        for (int i = 0; i < 3; i++)
        {
            String ruleId = "RULE_ID_" + (i + 1);
            ItemSubstitution itemSubstitution = item.createItemSubstitution(entityManager, ruleId);
            itemSubstitution.setProperty(ItemSubstitutionBase.Fields.SUBSTITUTION_TYPE.getCode(),
                    ItemSubstitutionType.IN_ADDITION);
            itemSubstitution.setProperty(
                    ItemSubstitutionBase.Fields.SUBSTITUTION_STATUS.getCode(),
                    ActiveStatus.INACTIVE);
            itemSubstitution.setProperty(ItemSubstitutionBase.Fields.START_DATE.getCode(),
                    LocalDate.now());
            itemSubstitution.setProperty(ItemSubstitutionBase.Fields.END_DATE.getCode(),
                    LocalDate.now());
            itemSubstitution.setProperty(ItemSubstitutionBase.Fields.BASE_ITEM_QTY.getCode(),
                    3.5);
            itemSubstitution.setProperty(ItemSubstitutionBase.Fields.DESCRIPTION.getCode(),
                    "MyDescr");

            for (int j = 0; j < 1; j++)
            {
                ItemSubstitutionDetail itemSubstitutionDetail = itemSubstitution.createItemSubstitutionDetail(entityManager, item);
                itemSubstitutionDetail.setProperty(
                        ItemSubstitutionDetailBase.Fields.SUBSTITUTION_STATUS.getCode(),
                        ActiveStatus.INACTIVE);
                itemSubstitutionDetail.setProperty(
                        ItemSubstitutionDetailBase.Fields.RANK.getCode(), j);
                itemSubstitutionDetail.setProperty(
                        ItemSubstitutionDetailBase.Fields.ITEM_QTY_RATIO.getCode(), 4.5);
            }
        }
    }

    private void checkFullItem(Item item, String expectedBrand)
    {
        Assert.assertEquals(item.getDescription(), "MyDesc");
        Assert.assertEquals(item.getLongDescription(), "MyLongDesc");
        Assert.assertEquals(item.getShortDescription(), "MyShortDesc");
        Assert.assertEquals(item.getBaseUOM(), UOM.UNITS);
        Assert.assertEquals(item.getSeason(), "Fall");
        Assert.assertEquals(item.getStyle(), "MyStyle");
        Assert.assertEquals(item.getColorCode(), "Red");
        Assert.assertEquals(item.getSize(), "XXL");
        Assert.assertEquals(item.getUpc(), "abcd");
        Assert.assertEquals(item.getEan(), "xyz");
        Assert.assertEquals(item.getGttn(), "gttn");
        Assert.assertEquals(item.getWeight(), new Quantity(25, UOM.UNITS));
        Assert.assertEquals(item.getVolume(), new Quantity(5, UOM.GALLONS));
        Assert.assertEquals(item.getDimension(), new Dimension(3, 4, 5, UOM.UNITS));
        Assert.assertEquals(item.getCatchWeightFlag(), Boolean.TRUE);
        Assert.assertEquals(item.getStatus(), ItemStatus.STATUS_THREE);
        Assert.assertEquals(item.getItemLargeImageUri(), "largeURI");
        Assert.assertEquals(item.getItemSmallImageUri(), "smallURI");
        Assert.assertEquals(item.getColorDescription(), "RedDesc");
        Assert.assertEquals(item.getSoldOnline(), Boolean.FALSE);
        Assert.assertEquals(item.getSoldInStores(), Boolean.FALSE);
        Assert.assertEquals(item.getPickupAtStore(), Boolean.FALSE);
        Assert.assertEquals(item.getShipToStore(), Boolean.FALSE);
        Assert.assertEquals(item.getIsReturnable(), Boolean.FALSE);
        Assert.assertEquals(item.getIsExchangeable(), Boolean.FALSE);
        Assert.assertEquals(item.getIsSubstitutable(), Boolean.FALSE);
        Assert.assertEquals(item.getWebsiteUrl(), "websiteUrl");
        Assert.assertEquals(item.getPriceStatus(), PriceStatus.STATUS_THREE);
        Assert.assertEquals(item.getSellThrough(), 256.78, .001);
        Assert.assertEquals(item.getBrand(), expectedBrand);
        Assert.assertEquals(item.getSizeSequence(), new Integer(4));
        Assert.assertEquals(item.getColorSequence(), new Integer(3));

        Assert.assertEquals(item.getCreatedBy(), context.getUser());
        Assert.assertEquals(item.getUpdatedBy(), context.getUser());
//        Assert.assertNotNull(item.getCreatedTimestamp());
//        Assert.assertNotNull(item.getUpdatedTimestamp());

        Assert.assertNotNull(item.getMediaList());
        Assert.assertEquals(item.getMediaList().size(), 3);
        for (int i = 0; i < item.getMediaList().size(); i++)
        {
            ItemMedia itemMedia = item.getMediaList().get(i);
            Assert.assertEquals(itemMedia.getMediaId(), "URI_ID_" + (i + 1));
            Assert.assertEquals(itemMedia.getUri(), "URI_" + (i + 1));
            Assert.assertEquals(itemMedia.getMimeType(), "image/bmp");
            Assert.assertSame(itemMedia.getMediaSize(), MediaSize.LARGE);
            Assert.assertEquals(itemMedia.getExtended("testCol"), "ABC");

        }

        for (int i = 0; i < item.getSubstitutionList().size(); i++)
        {
            ItemSubstitution itemSubstitution = item.getSubstitutionList().get(i);
            Assert.assertEquals(itemSubstitution.getRuleId(), "RULE_ID_" + (i + 1));
            Assert.assertEquals(itemSubstitution.getDescription(), "MyDescr");
            Assert.assertEquals(itemSubstitution.getBaseItemQty(), 3.5, .0001);
            Assert.assertEquals(itemSubstitution.getStartDate(), LocalDate.now());
            Assert.assertEquals(itemSubstitution.getEndDate(), LocalDate.now());
            Assert.assertSame(itemSubstitution.getSubstitutionStatus(), ActiveStatus.INACTIVE);
            Assert.assertSame(itemSubstitution.getSubstitutionType(),
                    ItemSubstitutionType.IN_ADDITION);

            Assert.assertNotNull(itemSubstitution.getSubstitutionDetailList());
            Assert.assertEquals(itemSubstitution.getSubstitutionDetailList().size(), 1);
            for (int j = 0; j < itemSubstitution.getSubstitutionDetailList().size(); j++)
            {
                ItemSubstitutionDetail detail = itemSubstitution.getSubstitutionDetailList().get(j);
                Assert.assertEquals(detail.getItemQtyRatio(), 4.5, .0001);
                Assert.assertSame(detail.getSubstitutionStatus(), ActiveStatus.INACTIVE);
                Assert.assertEquals(detail.getRank(), (Integer)j);
            }
        }


    }

    private void checkFullItemUsingProperties(Item item, String expectedBrand)
    {
        Assert.assertEquals(item.getProperty(ItemBase.Fields.DESCRIPTION.getCode()),"MyDesc");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.LONG_DESCRIPTION.getCode()), "MyLongDesc");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SHORT_DESCRIPTION.getCode()),"MyShortDesc");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.BASE_UOM.getCode()),UOM.UNITS);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SEASON.getCode()),"Fall");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.STYLE.getCode()),"MyStyle");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.COLOR_CODE.getCode()),"Red");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SIZE.getCode()), "XXL");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.UPC.getCode()),"abcd");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.EAN.getCode()),"xyz");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.GTTN.getCode()),"gttn");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.WEIGHT.getCode()),new Quantity(25, UOM.UNITS));
        Assert.assertEquals(item.getProperty(ItemBase.Fields.VOLUME.getCode()),new Quantity(5, UOM.GALLONS));
        Assert.assertEquals(item.getProperty(ItemBase.Fields.DIMENSION.getCode()),new Dimension(3, 4, 5, UOM.UNITS));
        Assert.assertEquals(item.getProperty(ItemBase.Fields.CATCH_WEIGHT_FLAG.getCode()),Boolean.TRUE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.STATUS.getCode()),ItemStatus.STATUS_THREE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.LARGE_IMAGE_URI.getCode()),"largeURI");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SMALL_IMAGE_URI.getCode()),"smallURI");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.COLOR_DESCRIPTION.getCode()),"RedDesc");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SOLD_ONLINE.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SOLD_IN_STORES.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.PICKUP_AT_STORE.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SHIP_TO_STORE.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.IS_RETURNABLE.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.IS_EXCHANGEABLE.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.IS_SUBSTITUTABLE.getCode()),Boolean.FALSE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.WEBSITE_URL.getCode()),"websiteUrl");
        Assert.assertEquals(item.getProperty(ItemBase.Fields.PRICE_STATUS.getCode()),PriceStatus.STATUS_THREE);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SELL_THROUGH.getCode()),256.78);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.BRAND.getCode()), expectedBrand);
        Assert.assertEquals(item.getProperty(ItemBase.Fields.SIZE_SEQUENCE.getCode()), new Integer(4));
        Assert.assertEquals(item.getProperty(ItemBase.Fields.COLOR_SEQUENCE.getCode()), new Integer(3));

        Assert.assertEquals(item.getCreatedBy(), context.getUser());
        Assert.assertEquals(item.getUpdatedBy(), context.getUser());
//        Assert.assertNotNull(item.getCreatedTimestamp());
//        Assert.assertNotNull(item.getUpdatedTimestamp());

        Assert.assertNotNull(item.getMediaList());
        Assert.assertEquals(item.getMediaList().size(), 3);
        for (int i = 0; i < item.getMediaList().size(); i++)
        {
            ItemMedia itemMedia = item.getMediaList().get(i);
            Assert.assertEquals(itemMedia.getProperty(
                    ItemMediaBase.Fields.MEDIA_ID.getCode()), "URI_ID_" + (i + 1));
            Assert.assertEquals(itemMedia.getProperty(ItemMediaBase.Fields.URI.getCode()), "URI_" + (i + 1));
            Assert.assertEquals(itemMedia.getProperty(ItemMediaBase.Fields.MIME_TYPE.getCode()), "image/bmp");
            Assert.assertSame(itemMedia.getProperty(ItemMediaBase.Fields.MEDIA_SIZE.getCode()), MediaSize.LARGE);
            Assert.assertSame(itemMedia.getProperty("testCol"), "XYZ");
        }

        for (int i = 0; i < item.getSubstitutionList().size(); i++)
        {
            ItemSubstitution itemSubstitution = item.getSubstitutionList().get(i);
            Assert.assertEquals(itemSubstitution.getProperty(
                    ItemSubstitutionBase.Fields.RULE_ID.getCode()), "RULE_ID_" + (i + 1));
            Assert.assertEquals(itemSubstitution.getProperty(ItemSubstitutionBase.Fields.DESCRIPTION.getCode()), "MyDescr");
            Assert.assertEquals(itemSubstitution.getProperty(ItemSubstitutionBase.Fields.BASE_ITEM_QTY.getCode()), 3.5);
            Assert.assertEquals(itemSubstitution.getProperty(ItemSubstitutionBase.Fields.START_DATE.getCode()), LocalDate.now());
            Assert.assertEquals(itemSubstitution.getProperty(ItemSubstitutionBase.Fields.END_DATE.getCode()), LocalDate.now());
            Assert.assertSame(itemSubstitution.getProperty(ItemSubstitutionBase.Fields.SUBSTITUTION_STATUS.getCode()), ActiveStatus.INACTIVE);
            Assert.assertSame(itemSubstitution.getProperty(ItemSubstitutionBase.Fields.SUBSTITUTION_TYPE.getCode()), ItemSubstitutionType.IN_ADDITION);

            Assert.assertNotNull(itemSubstitution.getSubstitutionDetailList());
            Assert.assertEquals(itemSubstitution.getSubstitutionDetailList().size(), 1);
            for (int j = 0; j < itemSubstitution.getSubstitutionDetailList().size(); j++)
            {
                ItemSubstitutionDetail detail = itemSubstitution.getSubstitutionDetailList().get(j);
                Assert.assertEquals(detail.getProperty(ItemSubstitutionDetailBase.Fields.ITEM_QTY_RATIO.getCode()), 4.5);
                Assert.assertSame(detail.getProperty(ItemSubstitutionDetailBase.Fields.SUBSTITUTION_STATUS.getCode()),
                        ActiveStatus.INACTIVE);
                Assert.assertEquals(detail.getProperty(ItemSubstitutionDetailBase.Fields.RANK.getCode()), j);
            }
        }


    }

    private void assertInTransaction()
    {
        Assert.assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
    }

    private void assertNotInTransaction()
    {
        Assert.assertFalse(TransactionSynchronizationManager.isActualTransactionActive());
    }


}
