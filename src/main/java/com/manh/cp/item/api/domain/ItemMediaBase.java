/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.api.domain;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import com.manh.cp.fw.entity.domain.api.CPAuditableEntity;
import com.manh.cp.item.impl.domain.enums.MediaSize;

/**
 * Interface for the ItemMedia entity, which has a many-to-one relationship with Item.
 */
@Generated(value = "Generated by EntityFramework")
public interface ItemMediaBase extends CPAuditableEntity
{
    public String ENTITY_NAME = "ItemMedia";

    public enum Fields
    {
        PK("PK"),
        MEDIA_ID("MediaID"),
        MIME_TYPE("MimeType"),
        MEDIA_SIZE("MediaSize"),
        URI("URI");

        private final String jsonCode;

        private Fields(String jsonCode)
        {
            this.jsonCode = jsonCode;
        }

        public String getCode()
        {
            return this.jsonCode;
        }

        public static List<String> getCodes()
        {
            List<String> result = new ArrayList<String>();
            for (Fields field : values())
            {
                result.add(field.getCode());
            }
            return result;
        }
    }
    /**
     * Get the Id for the URI. This, together with the parent Item, is unique.
     * Will not be null.
     * @return String URI for the Id
     */
    String getMediaId();

    /**
     * Get the parent Item
     * @return Item parent item
     */
    Item getParentItem();

    /**
     * Get the Mime type for the uri.
     * @return String mime type
     */
    String getMimeType();

    /**
     * Set the Mime type for the uri.
     * @param mimeType String mime type
     */
    void setMimeType(String mimeType);

    /**
     * Get the size for this media
     * @return MediaSize enum
     */
    MediaSize getMediaSize();

    /**
     * Set the size for this media.
     * @param mediaSize MediaSize enum
     */
    void setMediaSize(MediaSize mediaSize);

    /**
     * Get the URI itself.
     * May be null (although I don't know why it would.)
     * @return String URI
     */
    String getUri();

    /**
     * Set the URI itself.
     * @param uri String URI
     */
    void setUri(String uri);
}
