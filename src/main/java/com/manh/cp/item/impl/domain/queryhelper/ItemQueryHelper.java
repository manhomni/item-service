/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.item.impl.domain.queryhelper;

import com.manh.cp.fw.entity.domain.entity.CPEntityBase;
import com.manh.cp.fw.entity.domain.metadata.CPBasicFieldMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPMetadata;
import com.manh.cp.item.api.domain.Item;
import org.springframework.stereotype.Component;

import com.manh.cp.fw.entity.CPContext;
import com.manh.cp.fw.query.expressiontree.Condition;
import com.manh.cp.fw.query.expressiontree.Expression;
import com.manh.cp.fw.query.expressiontree.ExpressionNode;
import com.manh.cp.fw.query.expressiontree.LogicalOperator;
import com.manh.cp.item.impl.domain.entity.ItemEntity;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.PathBuilder;

import javax.persistence.metamodel.Attribute;

@Component
public class ItemQueryHelper
{

    public Predicate buildQuery(CPContext context, Expression expression)
    {
        PathBuilder<?> pathBuilder = new PathBuilder<ItemEntity>(ItemEntity.class, "itemEntity");
        ExpressionNode rootNode = expression.getRoot();
        return build(context, rootNode, pathBuilder);
    }

    private Predicate build(CPContext context, ExpressionNode node, PathBuilder<?> pathBuilder)
    {
        if (node instanceof Condition)
        {
            return buildCondition(context, (Condition) node, pathBuilder);
        }
        else if (node instanceof LogicalOperator)
        {
            return buildLogicalOperator(context, (LogicalOperator) node, pathBuilder);
        }
        else
        {
            throw new UnsupportedOperationException("Unsupported node " + node);
        }
    }

    private Predicate buildCondition(CPContext context, Condition condition,
            PathBuilder<?> pathBuilder)
    {
        // FIXME - this is currently assuming queries going against Item metadata. This needs to
        // be genericized.
        CPMetadata metadata = CPEntityBase.getMetadata(Item.ENTITY_NAME);
        if ((metadata != null) &&
                (metadata.getField(condition.getField()).getAttributeType() == Attribute.PersistentAttributeType.BASIC))
        {
            CPBasicFieldMetadata fieldMetadata = (CPBasicFieldMetadata)metadata.getField(condition.getField());
            if (fieldMetadata.getFieldClass().equals(Integer.class))
            {
                System.out.println(condition.getOperator());
                NumberPath<Integer> integerPath = pathBuilder.getNumber(fieldMetadata.getFieldName(),
                        Integer.class);
                Integer value = Integer.parseInt(condition.getValue());
                return integerPath.eq(value);
            }
            return pathBuilder.get(fieldMetadata.getFieldName()).eq(condition.getValue());
        }
        return null;
    }

    private Predicate buildLogicalOperator(CPContext context, LogicalOperator operator,
            PathBuilder<?> pathBuilder)
    {
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        operator.getChildNodes()
                .stream()
                .forEach(
                        (childNode) -> {
                            Predicate childPredicate = build(context, childNode, pathBuilder);
                            if (operator.getLogicalOperator().equalsIgnoreCase("AND"))
                            {
                                booleanBuilder.and(childPredicate);
                            }
                            else if (operator.getLogicalOperator().equalsIgnoreCase("OR"))
                            {
                                booleanBuilder.or(childPredicate);
                            }
                            else
                            {
                                throw new UnsupportedOperationException("Unsupported operator "
                                        + operator.getLogicalOperator());
                            }
                        });
        return booleanBuilder;
    }

}
