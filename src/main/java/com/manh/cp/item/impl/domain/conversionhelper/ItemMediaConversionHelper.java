/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.impl.domain.conversionhelper;

import com.manh.cp.item.api.domain.ItemMedia;
import com.manh.cp.item.api.domain.ItemMediaBase;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class ItemMediaConversionHelper
{

    private static Map<String, BiConsumer<ItemMedia, Object>> unmarshallOverrides = new HashMap<>();

    static
    {
    }

    private static Map<String, BiFunction<ItemMedia, Object, ?>> marshallOverrides = new HashMap<>();

    static
    {
    }

    //merges the properties passed and also if there is a child collection
    //will merge the child collection
    public static void merge(Map<String, Object> inputMap, ItemMedia itemMedia)
    {

        itemMedia.pushMergeMap(inputMap);
        inputMap.forEach((name, value) -> {
            setProperty(itemMedia, name, value);
        });
        itemMedia.popMergeMap();
    }

    private static Map<String, Object> defaultProperties = new HashMap<>();

    static
    {
        defaultProperties.put(ItemMediaBase.Fields.MEDIA_ID.getCode(), null);
        defaultProperties.put(ItemMediaBase.Fields.MIME_TYPE.getCode(), null);
        defaultProperties.put(ItemMediaBase.Fields.MEDIA_SIZE.getCode(), null);
        defaultProperties.put(ItemMediaBase.Fields.URI.getCode(), null);
    }

    public static Object getProperty(ItemMedia itemMedia, String name, Object template)
    {
        //will use reflection methods to set the basic properties
        //if the get property was overridden by generated class or by external
        //override then call the command and return
        if (marshallOverrides.containsKey(name))
        {
            return marshallOverrides.get(name).apply(itemMedia, template);
        }
        else
        {
            return itemMedia.getProperty(name);
        }
    }

    public static void setProperty(ItemMedia itemMedia, String name, Object value)
    {

        //if the set property was overridden by generated class or by external
        //override then call the command and return
        if (unmarshallOverrides.get(name) != null)
        {
            unmarshallOverrides.get(name).accept(itemMedia, value);
        }
        else
        {
            itemMedia.setProperty(name, value);
        }
    }

    public static Map<String, Object> getMap(ItemMedia itemMedia, Map<String, Object> template)
    {

        if (template == null || template.size() == 0)
        {
            //pass back the basic & default properties of the object back
            template = defaultProperties;
        }
        Map<String, Object> output = new HashMap<>();
        template.forEach((name, value) -> {
                    Object property = getProperty(itemMedia, name, value);
                    output.put(name, property);
                }
        );
        return output;
    }
}
