/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.impl.domain.conversionhelper;

import com.manh.cp.fw.entity.support.CPAction;
import com.manh.cp.fw.entity.support.helper.CPEntityConversionHelper;
import com.manh.cp.item.api.domain.Item;
import com.manh.cp.item.api.domain.ItemBase;
import com.manh.cp.item.api.domain.ItemMedia;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class ItemConversionHelper
{

    //
    private static Map<String, BiConsumer<Item, Object>> unmarshallOverrides = new HashMap<>();

    static
    {
        unmarshallOverrides.put("b", ItemConversionHelper::mergeItemMedia);
    }

    private static Map<String, BiFunction<Item, Object, ?>> marshallOverrides = new HashMap<>();

    static
    {
        marshallOverrides.put("b", ItemConversionHelper::getItemMedia);
    }

    //setting this up just for testing.....needs to be generated with
    //basic attributes in default properties
    private static Map<String, Object> defaultProperties = new HashMap<>();

    static
    {
        defaultProperties.put(ItemBase.Fields.PK.getCode(), null);
        defaultProperties.put(ItemBase.Fields.ORGANIZATION_ID.getCode(), null);
        defaultProperties.put(ItemBase.Fields.ITEM_ID.getCode(), null);
        defaultProperties.put(ItemBase.Fields.DESCRIPTION.getCode(), null);
//        defaultProperties.put("customerName", null);
//        defaultProperties.put("additionalAttributes", null);
        Map<String, Object> childMap = new HashMap<>();
        childMap.put("itemId", null);
        defaultProperties.put("b",
                childMap); //should have been in orderline...just doing it for testing
    }

    /*Common functions that would be generated in all the conversion helpers
     * Look into moving them to item common class with generics. Need to think through
     * how to give access to the marshalling/unmarshalling override maps outside
     * this class. It may be ok to repeat for now. Next iteration, will move these
     * general functions to EntityConversionHelper
     */
    public static Map<String, Object> getMap(Item item, Map<String, Object> template)
    {

        if (template == null || template.size() == 0)
        {
            //pass back the basic & default properties of the object back
            template = defaultProperties;
        }
        Map<String, Object> output = new HashMap<>();
        template.forEach((name, value) -> {
                    Object property = getProperty(item, name, value);
                    output.put(name, property);
                }
        );
        return output;
    }

    public static Object getProperty(Item item, String name, Object template)
    {
        //will use reflection methods to set the basic properties
        //if the get property was overridden by generated class or by external
        //override then call the command and return
        if (marshallOverrides.containsKey(name))
        {
            return marshallOverrides.get(name).apply(item, template);
        }
        else
        {
            return item.getProperty(name);
        }
    }

    public static void setProperty(Item item, String name, Object value)
    {

        //if the set property was overridden by generated class or by external
        //override then call the command and return
        if (unmarshallOverrides.get(name) != null)
        {
            unmarshallOverrides.get(name).accept(item, value);
        }
        else
        {
            item.setProperty(name, value);
        }
    }

    public static void merge(Map<String, Object> input, Item item)
    {

        item.pushMergeMap(input);
        input.forEach((name, value) -> {
                    setProperty(item, name, value);
                }
        );
        item.popMergeMap();
    }

    /////////Beginning of specific functions that override properties.
    //All collection fetch is overridden

    private static List<Map<String, Object>> getItemMedia(Item a, Object template)
    {

        return CPEntityConversionHelper.<ItemMedia>getChildCollection(a.getMediaList(), template,
                ItemMediaConversionHelper::getMap);
    }

    //merge the child collection
    private static void mergeItemMedia(Item item, Object b)
    {

        CPEntityConversionHelper.<Item, ItemMedia> mergeChildCollection(item, item.getMediaList(), b,
                item::getItemMedia, ItemMediaConversionHelper::merge,
                CPAction.RESET);
    }

}


