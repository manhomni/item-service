/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.impl.domain.converter;

import com.manh.cp.item.impl.domain.enums.ItemSubstitutionType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ItemSubstitutionTypeConverter implements AttributeConverter<ItemSubstitutionType, String>
{
    @Override
    public String convertToDatabaseColumn(ItemSubstitutionType status)
    {
        return (status == null) ? ItemSubstitutionType.WHEN_OUT.getCode() : status.getCode();
    }

    @Override
    public ItemSubstitutionType convertToEntityAttribute(String dbType)
    {
        return ItemSubstitutionType.fromCode(dbType);
    }
}
