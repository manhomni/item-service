/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.item.impl.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.manh.cp.fw.entity.CPContext;
import com.manh.cp.fw.entity.domain.entity.CPEntityMap;
import com.manh.cp.fw.query.Query;
import com.manh.cp.item.api.domain.Item;
import com.manh.cp.item.api.service.ItemService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@RequestMapping("/api/itemservice")
@Api(basePath = "/api/itemservice" , value = "items", description = "Item API")
public class ItemControllerBase
{

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/item/{pk}", produces = { MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE })
    @ApiOperation(value = "/item/{pk}",  notes = "Get the Item")
    public ResponseEntity<CPEntityMap> getEntity(@PathVariable String pk,
            @RequestParam(required = false) String templateName)
    {
        // FIXME Add the user information
        CPContext context = new CPContext("dummy");
        CPEntityMap entityMap = new CPEntityMap();
        entityMap.setEntity("item");

        Map<String, Object> itemMap = itemService.getItem(context, Long.parseLong(pk), null);
        entityMap.getMap().putAll(itemMap);

        HttpHeaders responseHeaders = new HttpHeaders();
        // TODO Add headers
        ResponseEntity<CPEntityMap> response = new ResponseEntity<CPEntityMap>(entityMap,
                responseHeaders, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST)
    @ApiOperation(value = "/item",  notes = "Create the Item")
    public ResponseEntity<CPEntityMap> createEntity(@RequestBody CPEntityMap entityMap,
            @RequestParam(required = false) String templateName)
    {
        // FIXME Add the user information
        CPContext context = new CPContext("dummy");
        entityMap.setEntity("item");

        Map<String, Object> map = itemService.createItem(context, entityMap.getMap(), null);
        entityMap.getMap().clear();
        entityMap.getMap().putAll(map);
        HttpHeaders responseHeaders = new HttpHeaders();
        // TODO Add headers
        ResponseEntity<CPEntityMap> response = new ResponseEntity<CPEntityMap>(entityMap,
                responseHeaders, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/item/{pk}", method = RequestMethod.PUT)
    @ApiOperation(value = "/item/{pk}",  notes = "Update the Item")
    public ResponseEntity<CPEntityMap> updateEntity(@PathVariable String pk,
            @RequestBody CPEntityMap entityMap, @RequestParam(required = false) String templateName)
    {
        // FIXME Add the user information
        CPContext context = new CPContext("dummy");
        entityMap.setEntity(Item.ENTITY_NAME);

        // PK given in the URL takes the precedence.
        Long pkLong = Long.parseLong(pk);
        entityMap.put(Item.Fields.PK.getCode(), pkLong);

        Map<String, Object> map = itemService.updateItem(context, entityMap.getMap(), null);
        entityMap.getMap().clear();
        entityMap.getMap().putAll(map);
        HttpHeaders responseHeaders = new HttpHeaders();
        // TODO Add headers
        ResponseEntity<CPEntityMap> response = new ResponseEntity<CPEntityMap>(entityMap,
                responseHeaders, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/item/search", produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.GET)
    @ApiOperation(value = "/item/search",  notes = "Search the Item")
    public ResponseEntity<List<CPEntityMap>> listItems(@RequestParam(required = true) String query,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "20") Integer size,
            @RequestParam(required = false, defaultValue = "ItemId") String sort,
            @RequestParam(required = false) String templateName)
    {
        // FIXME Add the user information
        CPContext context = new CPContext("dummy");

        Query criteriaQuery = new Query(query, page, size);
        List<CPEntityMap> itemList = itemService.listItems(context, criteriaQuery, null);

        HttpHeaders responseHeaders = new HttpHeaders();
        // TODO Add headers
        ResponseEntity<List<CPEntityMap>> response = new ResponseEntity<List<CPEntityMap>>(
                itemList, responseHeaders, HttpStatus.OK);
        return response;
    }

}
