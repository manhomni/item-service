/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.item.impl.domain.entity;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.manh.cp.fw.entity.support.factory.EntityFactory;

@Entity
@Table(name = "ITEM_SUBSTITUTION_DETAIL", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "SUBST_ORGANIZATION_ID", "SUBST_ITEM_ID",
                "ITEM_SUBST_PK" })
})
@Generated(value = "Generated once by EntityFramework")
public class ItemSubstitutionDetailEntity extends ItemSubstitutionDetailEntityBase
{
	protected ItemSubstitutionDetailEntity()
    {
        super();
    }

    public ItemSubstitutionDetailEntity(EntityFactory.FactoryToken token,
            ItemSubstitutionEntity parentItemSubstitution, ItemEntity substituteItem)
    {
        super(token, parentItemSubstitution, substituteItem);
    }

    /**
     * If fields are added to this subclass, they should be included in this toString() method.
     *
     * @return String representation of object
     */
    @Override public String toString()
    {
        return super.toString();
    }
}
