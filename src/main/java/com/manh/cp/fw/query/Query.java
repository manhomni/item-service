/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.fw.query;

import java.util.List;
import java.util.Map;

import com.manh.cp.fw.query.expressiontree.Expression;
import com.manh.cp.fw.query.parser.QueryParser;
import com.manh.cp.fw.query.parser.QueryParserImpl;

public class Query
{

    private String query;

    private int page;

    private int size;

    private List<Map<String, String>> sort;

    private Expression expression;

    public Query(String query, int page, int size)
    {
        this(query, page, size, null);
    }

    public Query(String query, int page, int size, List<Map<String, String>> sort)
    {
        super();
        this.query = query;
        this.page = page;
        this.size = size;
        this.sort = sort;
        parseQuery();
    }

    public Expression getExpression()
    {
        return expression;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
        parseQuery();
    }

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public List<Map<String, String>> getSort()
    {
        return sort;
    }

    public void setSort(List<Map<String, String>> sort)
    {
        this.sort = sort;
    }

    private void parseQuery()
    {
        QueryParser parser = new QueryParserImpl(getQuery());
        expression = parser.parse();
    }

}
