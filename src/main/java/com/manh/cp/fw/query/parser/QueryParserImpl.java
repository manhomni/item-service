/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.fw.query.parser;

import java.util.Stack;

import com.manh.cp.fw.query.expressiontree.Condition;
import com.manh.cp.fw.query.expressiontree.ConditionGroup;
import com.manh.cp.fw.query.expressiontree.Expression;
import com.manh.cp.fw.query.expressiontree.ExpressionNode;
import com.manh.cp.fw.query.expressiontree.LogicalOperator;

public class QueryParserImpl implements QueryParser
{

    Token currToken;

    Lexer lexer;

    boolean hasError = false;

    Condition currCondition = null;

    private Stack<ExpressionNode> operatorStack = new Stack<>();

    private Stack<ExpressionNode> operandStack = new Stack<>();

    public QueryParserImpl(String text)
    {
        lexer = new Lexer(text);
    }

    private void fieldName()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {
                String field = currToken.getText();
                currCondition.setField(field);
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }
    }

    private void lhs()
    {
        if (!hasError)
        {
            fieldName();
        }

    }

    private void quote()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.QUOTE))
            {
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }
    }

    private void date()
    {
        if (!hasError)
        {
            day();
            colon();
            hours();
            colon();
            minutes();
        }
    }

    private void minutes()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {

                String op = currCondition.getValue();
                op = op + currToken.getText();
                currCondition.setValue(op);

                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void colon()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.COLON))
            {
                String op = currCondition.getValue();
                op = op + currToken.getText();
                currCondition.setValue(op);
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void hours()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {
                String op = currCondition.getValue();
                op = op + currToken.getText();
                currCondition.setValue(op);
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void day()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {
                String op = currCondition.getValue();
                op = op + currToken.getText();
                currCondition.setValue(op);
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void dateOperator()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.PLUS)
                    || currToken.getType().equals(TokenType.MINUS))
            {
                String op = currCondition.getValue();
                op = op + currToken.getText();
                currCondition.setValue(op + " ");
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }
    }

    private void value()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {
                String op = currCondition.getValue();
                if (op == null)
                    op = currToken.getText();
                else
                    op = op + currToken.getText();
                currCondition.setValue(op);
                currToken = lexer.next();

            }
            else
            {
                hasError = true;
            }

        }
    }

    private void values()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {
                if (currToken.getText().equalsIgnoreCase("currentdate"))
                {
                    String op = currToken.getText();
                    currCondition.setValue(op + " ");
                    currToken = lexer.next();
                    dateOperator();
                    date();
                }
                else
                {
                    boolean endQuote = true;
                    while (endQuote)
                    {
                        if (!currToken.getType().equals(TokenType.QUOTE))
                        {
                            value();
                        }
                        else
                        {
                            endQuote = false;
                        }
                    }
                }
            }
            else
            {
                hasError = true;
            }
        }
    }

    private void rhs()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.NAME))
            {
                currCondition.setValue(currToken.getText());

                currToken = lexer.next();
            }
            else if (currToken.getType().equals(TokenType.QUOTE))
            {
                quote();
                // To handle a blank value condition.
                if (!currToken.getType().equals(TokenType.QUOTE))
                {
                    values();
                }
                quote();
            }
            else
            {
                hasError = true;
            }
        }
    }

    private void logicalOperator()
    {
        if (!hasError)
        {
            if ((currToken.getType().equals(TokenType.NAME)))
            {
                if (currToken.getText().equalsIgnoreCase("AND")
                        || currToken.getText().equalsIgnoreCase("OR"))
                {
                    handleOperator(currToken.getText());
                    currToken = lexer.next();
                }
                else
                {
                    hasError = true;
                }
            }
            else
            {
                hasError = true;

            }
        }
    }

    private void operator()
    {
        if (!hasError)
        {
            if ((currToken.getType().equals(TokenType.ASSIGN))
                    || (currToken.getType().equals(TokenType.GT)))
            {

                String op = currCondition.getOperator();
                op = op + currToken.getText();
                currCondition.setOperator(op);

                currToken = lexer.next();
            }
        }

    }

    private void multiOperator()
    {
        if (!hasError)
        {
            if ((currToken.getType().equals(TokenType.ASSIGN))
                    || (currToken.getType().equals(TokenType.BANG))
                    || (currToken.getType().equals(TokenType.LT))
                    || (currToken.getType().equals(TokenType.GT)))
            {
                currCondition.setOperator(currToken.getText());
                currToken = lexer.next();
                operator();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void condition()
    {
        if (currToken.getType().equals(TokenType.LEFT_PAREN))
        {
            lParen();
            complexCondition();
            rParen();

        }
        else
        {
            addCondition();
            lhs();
            multiOperator();
            rhs();
        }
    }

    private void rParen()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.RIGHT_PAREN))
            {
                handleRParen();
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void lParen()
    {
        if (!hasError)
        {
            if (currToken.getType().equals(TokenType.LEFT_PAREN))
            {
                handleLParen();
                currToken = lexer.next();
            }
            else
            {
                hasError = true;
            }
        }

    }

    private void complexCondition()
    {

        condition();

        boolean cont = true;
        while (cont)
        {
            if (currToken.getType().equals(TokenType.NAME)
                    && (currToken.getText().equalsIgnoreCase("AND") || currToken.getText()
                            .equalsIgnoreCase("OR")))
            {
                logicalOperator();
                condition();

            }
            else
            {
                cont = false;
            }
        }
    }

    @Override
	public Expression parse() throws ParseException
    {
        currToken = lexer.next();
        complexCondition();
        if (this.hasError)
            throw new ParseException("Exception during condition parsing");
        return finalizeStack();

    }

    private void handleOperator(String logicalOperator)
    {
        LogicalOperator op = new LogicalOperator(logicalOperator);
        operatorStack.push(op);
    }

    private void addCondition()
    {
        Condition condition = new Condition();
        this.currCondition = condition;
        operandStack.push(condition);
    }

    private void handleLParen()
    {
        ConditionGroup node = new ConditionGroup();
        operatorStack.push(node);
    }

    private void handleRParen()
    {
        attachOperator();

    }

    private Expression finalizeStack()
    {
        ExpressionNode operator = attachOperator();
        Expression tree = new Expression();
        tree.setRoot(operator);
        return tree;
    }

    private ExpressionNode attachOperator()
    {

        if (operatorStack.empty())
        {
            return operandStack.pop();
        }
        ExpressionNode operator = operatorStack.pop();
        if (operator instanceof ConditionGroup)
        {
            return null;
        }
        int count = 0;
        while (count < 2)
        {
            operator.addChild(operandStack.pop());
            count++;
        }
        operandStack.push(operator);

        if (!operatorStack.empty())
            attachOperator();
        return operator;
    }

    public boolean isHasError()
    {
        return hasError;
    }

}
