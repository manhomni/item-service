/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.fw.query.parser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Lexer implements Iterator<Token>
{
    /**
     * Creates a new Lexer to tokenize the given string.
     * 
     * @param text
     *            String to tokenize.
     */
    public Lexer(String text)
    {
        mIndex = 0;
        mText = text;

        // Register all of the TokenTypes that are explicit punctuators.
        for (TokenType type : TokenType.values())
        {
            Character punctuator = type.punctuator();
            if (punctuator != null)
            {
                mPunctuators.put(punctuator, type);
            }
        }
    }

    @Override
    public boolean hasNext()
    {
        return true;
    }

    @Override
    public Token next()
    {
        while (mIndex < mText.length())
        {
            char c = mText.charAt(mIndex++);

            if (c == '\'')
            {
                int start = mIndex;
                while (mIndex < mText.length())
                {
                    // If the next character is a quote and also if the previous
                    // character is not an escape character
                    char currentChar = mText.charAt(mIndex);
                    char previousChar = mText.charAt(mIndex - 1);
                    mIndex++;
                    if (currentChar == '\'' && previousChar != '\\')
                        break;
                }
                String name = mText.substring(start, mIndex - 1);
                //Replace the escaped characters
                name = name.replace("\\", "");
                return new Token(TokenType.NAME, name);
            }
            else if (mPunctuators.containsKey(c))
            {
                // Handle punctuation.
                return new Token(mPunctuators.get(c), Character.toString(c));
            }
            else if (Character.isLetter(c) || Character.isDigit(c))
            {
                // Handle names.
                int start = mIndex - 1;
                while (mIndex < mText.length())
                {
                    if (!(Character.isLetter(mText.charAt(mIndex)) || mText.charAt(mIndex) == '.' || Character
                            .isDigit(mText.charAt(mIndex))))
                        break;
                    mIndex++;
                }

                String name = mText.substring(start, mIndex);
                return new Token(TokenType.NAME, name);
            }
            else
            {
                // Ignore all other characters (whitespace, etc.)
            }
        }

        // Once we've reached the end of the string, just return EOF tokens.
        // We'll
        // just keeping returning them as many times as we're asked so that the
        // parser's lookahead doesn't have to worry about running out of tokens.
        return new Token(TokenType.EOF, "");
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    private final Map<Character, TokenType> mPunctuators = new HashMap<Character, TokenType>();

    public final String mText;

    public int mIndex = 0;

}