/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.fw.query.parser;

/**
 * 
 * This is an enum with all the token types
 * 
 * @author Baibhav Singh
 * 
 */

public enum TokenType
{
    LEFT_PAREN, RIGHT_PAREN, COMMA, ASSIGN, PLUS, MINUS, ASTERISK, SLASH, CARET, TILDE, BANG, QUESTION, COLON, NAME, GT, LT, QUOTE, EOF;

    /**
     * If the TokenType represents a punctuator (i.e. a token that can split an
     * identifier like '+', this will get its text.
     */
    public Character punctuator()
    {
        switch (this)
        {
            case LEFT_PAREN:
                return '(';
            case RIGHT_PAREN:
                return ')';
            case COMMA:
                return ',';
            case ASSIGN:
                return '=';
            case PLUS:
                return '+';
            case MINUS:
                return '-';
            case ASTERISK:
                return '*';
            case SLASH:
                return '/';
            case CARET:
                return '^';
            case TILDE:
                return '~';
            case BANG:
                return '!';
            case QUESTION:
                return '?';
            case COLON:
                return ':';
            case GT:
                return '>';
            case LT:
                return '<';
            case QUOTE:
                return '\'';

            default:
                return null;
        }
    }
}