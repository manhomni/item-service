/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class CPTemplate
{
    private final Set<String> fieldSet = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private final Map<String, CPTemplate> childMap = new ConcurrentHashMap<>();
    protected String entityName;

    @SuppressWarnings("unused")
    private CPTemplate()
    {
        // don't allow
    }

    public CPTemplate(String entityName)
    {
        this.entityName = entityName;
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void addField(String fieldName)
    {
        this.fieldSet.add(fieldName);
    }

    public boolean isFieldSet(String fieldName)
    {
        return this.fieldSet.contains(fieldName);
    }

    public void addChild(String fieldName, CPTemplate childTemplate)
    {
        this.childMap.put(fieldName, childTemplate);
    }

    public CPTemplate getChild(String fieldName)
    {
        return this.childMap.get(fieldName);
    }

    public boolean isChildSet(String fieldName)
    {
        return this.childMap.containsKey(fieldName) &&
                this.childMap.get(fieldName) != null;
    }
}
