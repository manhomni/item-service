/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.embeddable;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Embeddable;

import com.manh.cp.fw.entity.domain.enums.UOM;

@Cacheable
@Embeddable
public class Quantity implements Serializable
{
	private Double qty;

    private UOM uom;

    protected Quantity()
    {
    }

    public Quantity(long qty, UOM uom)
    {
        this.qty = (double) qty;
        this.uom = uom;
    }

    public Quantity(double qty, UOM uom)
    {
        this.qty = qty;
        this.uom = uom;
    }

    public UOM getUom()
    {
        return uom;
    }

    public void setUom(UOM uom)
    {
        this.uom = uom;
    }

    public Double getQty()
    {
        return qty;
    }

    public void setQty(Double qty)
    {
        this.qty = qty;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Quantity quantity = (Quantity) o;

        if (!qty.equals(quantity.qty))
            return false;
        if (uom != quantity.uom)
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = qty.hashCode();
        result = 31 * result + uom.hashCode();
        return result;
    }

    @Override public String toString()
    {
        return "Quantity{" +
                "qty=" + qty +
                ", uom=" + uom +
                '}';
    }
}
