/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.support.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.manh.cp.fw.entity.domain.api.CPEntity;
import com.manh.cp.fw.entity.exception.CPEntityException;
import com.manh.cp.fw.entity.support.CPAction;

/**
 * Static helper methods for marshalling/unmarshalling child collections.
 */
public class CPEntityConversionHelper
{
    private CPEntityConversionHelper()
    {
        // Private constructor to prevent instantiation
    }

    /**
     * Marshall a collection of child entities.
     *
     * @param dbChildList List of child CPEntity objects
     * @param template May be either a List of maps (of String/Object) or a Map (String/Object) itself  // FIXME
     * @param getChildMap BiFunction to invoke which will convert each CPEntity object into a Map of String/Object
     * @param <C> Type variable for the class
     * @return List of Map representations of the children
     */
    public static <C extends CPEntity> List<Map<String, Object>>
    getChildCollection(List<C> dbChildList, Object template,
            BiFunction<C, Map<String, Object>, Map<String, Object>> getChildMap)
    {

        //template passed is value associated with the child collection template object
        //1. Extract the property for this method

        //template could be null if it is basic property, a list or map if it a collection
        //of child objects or an embedded object
        //for child collection properties it will be a list or a map
        if (template != null && template instanceof List)
        {
            //get the first object from the template as this should be the map of child object
            template = ((List<?>) template).get(0);
        }
        if (!(template instanceof Map))
            template = null;

        //a list of maps will be returned for the child object, each map representing
        //the child entry
        @SuppressWarnings("unchecked")
		final Map<String, Object> finalTemplate = (Map<String, Object>) template;
        List<Map<String, Object>> retList = new ArrayList<>();
        dbChildList.forEach((child) -> {
            Map<String, Object> retMap = getChildMap.apply(child, finalTemplate);
            retList.add(retMap);
        });

        return retList;

    }


	/* Function used for unmarshalling input child collection in a Map format to
	 * child child objects.
	 * Functional interface getChild : provides method to get the child child object from parent
	 * Functional interface mergeChild: merges child object with the input map provided
	 */

    /**
     * Unmarshall a collection of child map representations back into the entity child collection.
     *
     * @param parent
     * @param dbChildList
     * @param toMergeCollection
     * @param getChild
     * @param mergeChild
     * @param action
     * @param <P> CPEntity type variable for the parent
     * @param <C> CPEntity type variable for the child
     */
    public static <P extends CPEntity, C extends CPEntity> void
    mergeChildCollection(P parent, List<C> dbChildList, Object toMergeCollection,
            Function<Map<String, Object>, C> getChild,
            BiConsumer<Map<String, Object>, C> mergeChild, CPAction action)
    {

        if (toMergeCollection == null)
        {
            return;
        }

        // Make sure we're dealing with a List
        List<Map<String, Object>> toMergeList = recastAsList(toMergeCollection);

        //if action is Reset, then run over all Children and mark the ones that are not passed
        //in input as "deleted". whether the child object is "soft" deleted or "hard" will depend on
        //child child
        if (action == CPAction.RESET)
        {
            CPEntityConversionHelper.<C>deleteChildObjectsOnReset(dbChildList, toMergeList);
        }

        for (Map<String, Object> m : toMergeList)
        {
            //find the child object from this parent. If child collection is large
            //parent child can decide to read individual children as needed and merge
            //especially useful for large order line situations
            C childEntity = getChild.apply(m);
            if (childEntity != null)
            {
                mergeChild.accept(m, childEntity); //invoke the Child merge function
            }
            else
            { //create new child object and add to the child collection
                createChildObject(parent, m);
            }
        }

    }

    @SuppressWarnings("unchecked")
	private static List<Map<String, Object>> recastAsList(Object toMergeCollection)
    {
        List<Map<String, Object>> toMergeList;

        //object b could be a list of child object or just one depending on the
        //json parser that was used to convert it to map. Will need to see if we can find a JSON parser that will
        //always create a list instead of Map when there is one child entry

        if (!(toMergeCollection instanceof List) && !(toMergeCollection instanceof Map))
        {
            throw new CPEntityException("Invalid toMergeCollection data type");
        }

        //switch everything to a list to make processing simpler
        if (toMergeCollection instanceof Map)
        {
            toMergeList = new ArrayList<>();
            toMergeList.add((Map<String, Object>) toMergeCollection);
        }
        else
        {
            toMergeList = (List<Map<String, Object>>) toMergeCollection;
        }
        return toMergeList;
    }

    private static <C extends CPEntity> void deleteChildObjectsOnReset(List<C> dbChildList,
            List<Map<String, Object>> toMergeList)
    {

        for (C child : dbChildList)
        {
            boolean match = false;
            for (Map<String, Object> m : toMergeList)
            {
                if (child.isMatch(m))
                {
                    match = true;
                    break;
                }
            }
            if (!match)
            {
                //TODO
                //child.setMarkForDelete(true); //set the delete marker. Child child implementation can decide hard or soft delete
            }
        }
    }

    private static <P extends CPEntity, C extends CPEntity> void createChildObject(P parent, Map<String, Object> m)
    {
        //create a managed child for the child collection from
        //TODO need to talk to Bill on how managed objects are created and plug that in

        // FIXME - the child object is created through an API on the parent. This needs to be genericized and pulled up
        //         to the CPEntity level so it can be called here.
    }

}
