/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.metamodel.Attribute;

import com.manh.cp.fw.entity.domain.entity.CPEntityBase;

public class CPMetadata
{
    private String entityName;
    private final Map<String, CPFieldMetadata> metadata = new ConcurrentHashMap<>();
    private CPRootTemplate defaultTemplate;
    private List<String> codes;

    @SuppressWarnings("unused")
    private CPMetadata()
    {
        // Don't use, have to give it a name
    }


    public CPMetadata(String entityName, List<String> codes)
    {
        this.entityName = entityName;
        this.codes = codes;
    }

    public String getEntityName()
    {
        return this.entityName;
    }

    public CPFieldMetadata getField(String jsonName)
    {
        return metadata.get(jsonName);
    }

    public void setField(String jsonName, CPFieldMetadata fieldMetadata)
    {
        this.metadata.put(jsonName, fieldMetadata);
        if (!this.codes.contains(jsonName))
        {
            this.codes.add(jsonName);
        }
    }

    private Map<String, CPFieldMetadata> getFieldMetadata()
    {
        return this.metadata;
    }

    public List<String> getCodes()
    {
        return this.codes;
    }

    public CPRootTemplate getDefaultTemplate()
    {
        if (defaultTemplate == null)
        {
            resetDefaultTemplate();
        }
        return defaultTemplate;
    }

    public void resetDefaultTemplate()
    {
        defaultTemplate = generateDefaultTemplate();
    }

    private CPRootTemplate generateDefaultTemplate()
    {
        CPRootTemplate template = new CPRootTemplate(entityName, "DEFAULT");
        populateTemplate(template, this);

        return template;
    }

    private void populateTemplate(CPTemplate template, CPMetadata metadata)
    {
        Map<String, CPFieldMetadata> fieldMetadataMap = metadata.getFieldMetadata();

        fieldMetadataMap.forEach((field, fieldMetadata) -> {
            if (fieldMetadata.isDefaultTemplate())
            {
                if (fieldMetadata.getAttributeType() == Attribute.PersistentAttributeType.BASIC)
                {
                    template.addField(field);
                }
                else if (fieldMetadata.getAttributeType() == Attribute.PersistentAttributeType.ONE_TO_MANY)
                {
                    String targetEntityName = ((CPOneToManyFieldMetadata)fieldMetadata).getTargetEntityName();
                    CPMetadata childMetadata = CPEntityBase.getMetadata(targetEntityName);
                    CPTemplate childTemplate = new CPTemplate(targetEntityName);
                    populateTemplate(childTemplate, childMetadata);
                    template.addChild(field, childTemplate);
                }
            }
        });
    }

}
