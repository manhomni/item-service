/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.sequence;

import java.util.Random;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.internal.databaseaccess.Accessor;
import org.eclipse.persistence.internal.sessions.AbstractSession;
import org.eclipse.persistence.sequencing.StandardSequence;
import org.eclipse.persistence.sessions.Session;

public class ManhSequence extends StandardSequence implements SessionCustomizer
{
	private static Random random = new Random();

    public ManhSequence()
    {
        super();
    }

    public ManhSequence(String name)
    {
        super(name);
    }

    @Override protected Number updateAndSelectSequence(Accessor accessor,
            AbstractSession writeSession, String seqName, int size)
    {
        return Math.abs(random.nextLong());
    }

    @Override public boolean shouldAcquireValueAfterInsert()
    {
        return false;
    }

    @Override public boolean shouldUseTransaction()
    {
        return false;
    }

    @Override
	public void customize(Session session) throws Exception
    {
        ManhSequence sequence = new ManhSequence("manhSequence");
        session.getLogin().addSequence(sequence);
    }
}
