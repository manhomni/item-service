/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.manh.cp.ManhContext;

public class CPContext implements Serializable
{
	private final String user;

    private final Map<String, Object> properties = new ConcurrentHashMap<>();

    private final Map<String, ManhContext> serviceContextMap = new ConcurrentHashMap<>();

    public CPContext(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }

    public void setProperty(String key, Object property)
    {
        this.properties.put(key, property);
    }

    public void removeProperty(String key)
    {
        this.properties.remove(key);
    }

    public Object getProperty(String key)
    {
        return this.properties.get(key);
    }

    public void setServiceContext(String serviceName, ManhContext serviceContext)
    {
        this.serviceContextMap.put(serviceName, serviceContext);
    }

    public ManhContext getServiceContext(String serviceName)
    {
        return serviceContextMap.get(serviceName);
    }
}
