/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CPOverride
{
    private Map<String, CPFieldOverride> overrides = new ConcurrentHashMap<>();

    public CPOverride()
    {
        // nothing to do
    }

    public CPFieldOverride getField(String jsonName)
    {
        return overrides.get(jsonName);
    }

    public void setField(String jsonName, CPFieldOverride fieldOverride)
    {
        this.overrides.put(jsonName, fieldOverride);
    }

}
