/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.support.helper;

import java.lang.reflect.Field;
import java.util.Map;

import org.eclipse.persistence.internal.jpa.metadata.accessors.classes.EntityAccessor;
import org.eclipse.persistence.internal.jpa.metadata.accessors.classes.XMLAttributes;
import org.eclipse.persistence.internal.jpa.metadata.accessors.mappings.BasicAccessor;
import org.eclipse.persistence.internal.jpa.metadata.xml.XMLEntityMappings;
import org.eclipse.persistence.jpa.metadata.XMLMetadataSource;
import org.eclipse.persistence.logging.SessionLog;

import com.manh.cp.fw.entity.domain.entity.CPEntityBase;
import com.manh.cp.fw.entity.domain.metadata.CPBasicFieldMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPMetadata;

public class CPEntityMappingMetadataSource extends XMLMetadataSource
{
    @Override
    public XMLEntityMappings getEntityMappings(Map<String, Object> properties,
            ClassLoader classLoader, SessionLog log)
    {
        XMLEntityMappings mappings = super.getEntityMappings(properties, classLoader, log);

        // We're going to read the eclipselink-orm.xml to determine which columns are "VIRTUAL".
        // For each of those, we're going to add entries to the getterMethodMap and setterMethodMap for the class,
        // so that they show up as "normal" properties, available for getProperty() and setProperty() calls.
        for (EntityAccessor entity : mappings.getEntities())
        {
            XMLAttributes attributes = entity.getAttributes();
            for (BasicAccessor basicAccessor : attributes.getBasics())
            {
                if (basicAccessor.getAccess().equalsIgnoreCase("VIRTUAL"))
                {
                    String name = basicAccessor.getName();
                    String className = entity.getClassName();
                    try
                    {
                        Class<?> clazz = Class.forName(className, true, classLoader);

                        CPBasicFieldMetadata.Builder builder = new CPBasicFieldMetadata.Builder(name, clazz);
                        builder = builder.extendedAttribute(true);
                        builder = builder.fieldName(name); // For extended attributes, the field name is the same
                        builder = builder.fieldClass(getJavaClass(basicAccessor.getAttributeType()));
                        builder = builder.getterMethod("getExtended");
                        builder = builder.setterMethod("setExtended");

                        CPBasicFieldMetadata fieldMetadata = builder.build();

                        Field field = clazz.getField("ENTITY_NAME");
                        String entityName = (String)field.get(String.class);
                        CPMetadata metadata = CPEntityBase.getMetadata(entityName);
                        metadata.setField(name, fieldMetadata);
                    }
                    catch (ClassNotFoundException | IllegalAccessException |
                            NoSuchFieldException e)
                    {
                        log.logThrowable(SessionLog.SEVERE, e);
                    }

                }
            }
        }

        return mappings;
    }

    private Class<?> getJavaClass(String attributeType)
    {
        // FIXME - support other types of extended attributes
        return String.class;
    }
}
