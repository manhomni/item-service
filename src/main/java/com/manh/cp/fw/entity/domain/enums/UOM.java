/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.enums;

public enum UOM
{
    UNITS("Units", "EA"),
    POUNDS("Pounds", "LB"),
    GALLONS("Gallons", "GL");

    private final String unit;
    private final String unitCode;

    UOM(String unit, String unitCode)
    {
        this.unit = unit;
        this.unitCode = unitCode;
    }

    public String getUnit()
    {
        return unit;
    }

    public String getUnitCode()
    {
        return unitCode;
    }

    public static UOM fromString(String unit)
    {
        if (unit != null)
        {
            for (UOM b : UOM.values())
            {
                if (unit.equalsIgnoreCase(b.unit))
                {
                    return b;
                }
            }
        }
        throw new IllegalArgumentException("No UOM with unit " + unit + " found");
    }

    public static UOM fromCode(String unitCode)
    {
        for (UOM b : UOM.values())
        {
            if (unitCode.equalsIgnoreCase(b.unitCode))
            {
                return b;
            }
        }
        throw new IllegalArgumentException("No UOM with unitCode " + unitCode + " found");
    }


}
