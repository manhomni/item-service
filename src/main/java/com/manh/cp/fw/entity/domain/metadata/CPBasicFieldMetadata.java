/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import java.lang.reflect.Method;

import javax.persistence.metamodel.Attribute;

public class CPBasicFieldMetadata extends CPFieldMetadata
{
    private String fieldName;
    private Method getterMethod;
    private Method setterMethod;
    private Class<?> fieldClass;
    private boolean isSearchable;
    private Object defaultValue;
    private boolean extendedAttribute;

    private CPBasicFieldMetadata()
    {
        // Must be built using Builder
    }

    private CPBasicFieldMetadata(Builder builder, Class<?> typeParameterClass)
    {
        super(builder.jsonName, builder.defaultTemplate);
        this.fieldName = builder.fieldName;
        try
        {
            if (builder.getterMethod != null)
            {
                if (builder.extendedAttribute)
                {
                    this.getterMethod = typeParameterClass.getMethod(builder.getterMethod, String.class);
                }
                else
                {
                    this.getterMethod = typeParameterClass.getMethod(builder.getterMethod);
                }
            }
            if (builder.setterMethod != null)
            {
                if (builder.extendedAttribute)
                {
                    this.setterMethod = typeParameterClass.getMethod(builder.setterMethod, String.class, Object.class);
                }
                else
                {
                    this.setterMethod = typeParameterClass.getMethod(builder.setterMethod, builder.fieldClass);
                }
            }
        }
        catch (SecurityException | NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }

        this.fieldClass = builder.fieldClass;
        this.isSearchable = builder.isSearchable;
        this.defaultValue = builder.defaultValue;
        this.extendedAttribute = builder.extendedAttribute;
    }


    public static class Builder
    {
        private final Class<?> typeParameterClass;
        private final String jsonName;
        private String fieldName;
        private String getterMethod;
        private String setterMethod;
        private Class<?> fieldClass;
        private boolean isSearchable;
        private Object defaultValue;
        private boolean extendedAttribute;
        private boolean defaultTemplate = true;

        public Builder(String jsonName, Class<?> typeParameterClass)
        {
            this.jsonName = jsonName;
            this.typeParameterClass = typeParameterClass;
        }

        public Builder fieldName(String fieldName)
        {
            this.fieldName = fieldName;
            return this;
        }

        public Builder getterMethod(String getterMethod)
        {
            this.getterMethod = getterMethod;
            return this;
        }

        public Builder setterMethod(String setterMethod)
        {
            this.setterMethod = setterMethod;
            return this;
        }

        public Builder fieldClass(Class<?> fieldClass)
        {
            this.fieldClass = fieldClass;
            return this;
        }

        public Builder isSearchable(boolean isSearchable)
        {
            this.isSearchable = isSearchable;
            return this;
        }

        public Builder defaultValue(Object defaultValue)
        {
            this.defaultValue = defaultValue;
            return this;
        }

        public Builder extendedAttribute(boolean extendedAttribute)
        {
            this.extendedAttribute = extendedAttribute;
            return this;
        }

        public Builder defaultTemplate(boolean defaultTemplate)
        {
            this.defaultTemplate = defaultTemplate;
            return this;
        }

        public CPBasicFieldMetadata build()
        {
            return new CPBasicFieldMetadata(this, typeParameterClass);
        }
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public Method getGetterMethod()
    {
        return getterMethod;
    }

    public Method getSetterMethod()
    {
        return setterMethod;
    }

    public Class<?> getFieldClass()
    {
        return fieldClass;
    }

    public boolean isSearchable()
    {
        return isSearchable;
    }

    public Object getDefaultValue()
    {
        return defaultValue;
    }

    public boolean isExtendedAttribute()
    {
        return extendedAttribute;
    }

    @Override
    public Attribute.PersistentAttributeType getAttributeType()
    {
        return Attribute.PersistentAttributeType.BASIC;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;

        CPBasicFieldMetadata that = (CPBasicFieldMetadata) o;

        if (extendedAttribute != that.extendedAttribute)
            return false;
        if (isSearchable != that.isSearchable)
            return false;
        if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null)
            return false;
        if (fieldClass != null ? !fieldClass.equals(that.fieldClass) : that.fieldClass != null)
            return false;
        if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null)
            return false;
        if (getterMethod != null ? !getterMethod.equals(that.getterMethod) : that.getterMethod != null)
            return false;
        if (setterMethod != null ? !setterMethod.equals(that.setterMethod) : that.setterMethod != null)
            return false;

        return true;
    }

    @Override public String toString()
    {
        return "CPBasicFieldMetadata{" +
                "fieldName='" + fieldName + '\'' +
                ", getterMethod=" + getterMethod +
                ", setterMethod=" + setterMethod +
                ", fieldClass=" + fieldClass +
                ", isSearchable=" + isSearchable +
                ", defaultValue=" + defaultValue +
                ", extendedAttribute=" + extendedAttribute +
                "} " + super.toString();
    }
}
