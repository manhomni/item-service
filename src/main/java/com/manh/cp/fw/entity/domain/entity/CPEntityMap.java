/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp.fw.entity.domain.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@JsonRootName(value = "ManhXML")
public class CPEntityMap implements Serializable
{
    private final HashMap<String, Object> map = new HashMap<String, Object>();

    private String entity = "entity";

    @JacksonXmlProperty(isAttribute = true)
    public String getEntity()
    {
        return entity;
    }

    public void setEntity(String entity)
    {
        this.entity = entity;
    }

    @JsonAnySetter
    public void put(String key, Object value)
    {
        map.put(key, value);
    }

    @JsonAnyGetter
    public Map<String, Object> getMap()
    {
        return map;
    }

    public void putAll(Map<String, Object> map)
    {
        this.map.putAll(map);
    }

    public Object get(String key)
    {
        return map.get(key);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("CPEntityMap [map=");
        builder.append(map);
        builder.append(", entity=");
        builder.append(entity);
        builder.append("]");
        return builder.toString();
    }

}
