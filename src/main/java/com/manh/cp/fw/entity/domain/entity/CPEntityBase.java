/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.entity;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.metamodel.Attribute;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.VirtualAccessMethods;
import org.eclipse.persistence.config.CacheIsolationType;
import org.springframework.data.domain.Persistable;

import com.manh.cp.fw.entity.domain.api.CPEntity;
import com.manh.cp.fw.entity.domain.listener.CPEntityListener;
import com.manh.cp.fw.entity.domain.metadata.CPBasicFieldMetadata;
import com.manh.cp.fw.entity.domain.metadata.CPMetadata;
import com.manh.cp.fw.entity.support.factory.EntityFactory;

@Cache(alwaysRefresh = true, isolation = CacheIsolationType.ISOLATED)
@MappedSuperclass
@VirtualAccessMethods(get = "getExtended", set = "setExtended")
@EntityListeners({ CPEntityListener.class })
public abstract class CPEntityBase implements CPEntity, Serializable, Persistable<Long>
{
	private static Map<String, CPMetadata> metadataMap = new ConcurrentHashMap<>();

    // All entities will have primary keys with the same name and generation
    @Id
    @GeneratedValue(generator = "manhSequence")
    @Column(name = "PK", nullable = false, length = 22)
    private Long pk;

    // All entities will be marked "new" until they are first persisted.
    @Transient
    private boolean newInstance = true;

    @Override
    public boolean isNew()
    {
        return this.newInstance;
    }

    public void isNew(boolean newInstance)
    {
        this.newInstance = newInstance;
    }

    // All entities provide a map of getters, setters and unique keys.
    @Transient
    private final Deque<Map<String, Object>> mergeStack = new ArrayDeque<>();

    protected abstract List<Set<String>> getUniqueKeys();

    protected abstract String getEntityName();

    @Override
	public void pushMergeMap(Map<String, Object> map)
    {
        mergeStack.push(map);
    }

    @Override
	public Map<String, Object> popMergeMap()
    {
        try
        {
            return mergeStack.pop();
        }
        catch (NoSuchElementException e)
        {
            return null;
        }
    }

    public Map<String, Object> getMergeMap()
    {
        return mergeStack.peekFirst();
    }

    // checks if the map has fields that matches with the unique key of the
    // entity object
    @Override
	public boolean isMatch(Map<String, Object> inputMap)
    {
        // run over unique keys in the map and see if there is match with this
        // object
        return this.getUniqueKeys().stream()
                .anyMatch(sList -> sList.stream().filter(key -> !inputMap.containsKey(key)).anyMatch(s -> {
                    return compare(this.getProperty(s), inputMap.get(s));
                }));
    }

    // matches with another base entity object to see if the unique keys are the
    // same
    @Override
	public boolean isMatch(CPEntity toMatch)
    {
        // run over unique keys in the map and see if there is match with this
        // object
        return this.getUniqueKeys().stream().anyMatch(sList -> sList.stream().anyMatch(s -> {
            return compare(this.getProperty(s), toMatch.getProperty(s));
        }));
    }

    private boolean compare(Object a1, Object a2)
    {
        if ((a1 != null && a2 == null) || (a1 == null && a2 != null))
        {
            return false;
        }
        if (a1 != null && !a1.equals(a2))
        {
            return false;
        }
        return true;
    }

    @Override
	public Object getProperty(String name)
    {
        Object result = null;
        try
        {
            CPMetadata metadata = metadataMap.get(getEntityName());
            if ((metadata != null) &&
                    (metadata.getField(name).getAttributeType() == Attribute.PersistentAttributeType.BASIC))
            {
                CPBasicFieldMetadata fieldMetadata = (CPBasicFieldMetadata)metadata.getField(name);
                if (fieldMetadata != null)
                {
                    Method method = fieldMetadata.getGetterMethod();

                    if (method != null)
                    {
                        if (fieldMetadata.isExtendedAttribute())
                        {
                            result = method.invoke(this, name);
                        }
                        else
                        {
                            result = method.invoke(this);
                        }
                    }
                }
            }
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            // FIXME - Log the exception or throw it?
        }

        return result;
    }

    @Override
	public void setProperty(String name, Object value)
    {
        try
        {
            CPMetadata metadata = metadataMap.get(getEntityName());
            if ((metadata != null) &&
                    (metadata.getField(name).getAttributeType() == Attribute.PersistentAttributeType.BASIC))
            {
                CPBasicFieldMetadata fieldMetadata = (CPBasicFieldMetadata)metadata.getField(name);
                if (fieldMetadata != null)
                {
                    Method method = fieldMetadata.getSetterMethod();
                    if (method != null)
                    {
                        if (fieldMetadata.isExtendedAttribute())
                        {
                            method.invoke(this, name, value);
                        }
                        else
                        {
                            method.invoke(this, value);
                        }
                    }
                }
            }
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            // FIXME - log the exception or throw it?
        }
    }

    public static CPMetadata getMetadata(String entityName)
    {
        return metadataMap.get(entityName);
    }

    public static void addMetadata(String entityName, CPMetadata metadata)
    {
        metadataMap.put(entityName, metadata);
    }

    // All entities support the addition of attributes
    @Transient
    private final Map<String, Object> extensions = new HashMap<>();

    @SuppressWarnings("unchecked")
	@Override
	public <T> T getExtended(String name)
    {
        return (T) extensions.get(name);
    }

    @Override
	public Object setExtended(String name, Object value)
    {
        return extensions.put(name, value);
    }

    protected CPEntityBase()
    {
    }

    public CPEntityBase(EntityFactory.FactoryToken token)
    {
        this();
    }

    @Override
    public Long getPk()
    {
        return pk;
    }

    public void setPk(Long pk)
    {
        this.pk = pk;
    }

}
