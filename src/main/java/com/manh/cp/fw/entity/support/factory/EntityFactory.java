/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.support.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.persistence.EntityManager;

import org.eclipse.persistence.sessions.UnitOfWork;
import org.eclipse.persistence.sessions.changesets.ObjectChangeSet;
import org.eclipse.persistence.sessions.changesets.UnitOfWorkChangeSet;

import com.manh.cp.fw.entity.domain.entity.CPEntityBase;
import com.manh.cp.fw.entity.exception.CPEntityException;

public class EntityFactory
{
    private static EntityFactory entityFactory = new EntityFactory();

    private final FactoryToken token = new FactoryToken();

    private EntityFactory()
    {
    }

    public static EntityFactory getFactory()
    {
        return entityFactory;
    }

    @SuppressWarnings("unchecked")
	public <T> T newEntity(EntityManager entityManager, Class<T> entityClass, Object... ctorArgs)
    {
        T result = null;

        Class<?>[] dtArray = new Class[ctorArgs.length + 1];
        Object[] argArray = new Object[ctorArgs.length + 1];

        dtArray[0] = FactoryToken.class;
        argArray[0] = token;

        for (int i = 0; i < ctorArgs.length; i++)
        {
            dtArray[i + 1] = ctorArgs[i].getClass();
            argArray[i + 1] = ctorArgs[i];
        }

        try
        {
            Constructor<T> ctor = entityClass.getConstructor(dtArray);

            T newEntity = ctor.newInstance(argArray);

            ((CPEntityBase)newEntity).isNew(true);

            UnitOfWork unitOfWork = entityManager.unwrap(UnitOfWork.class);
            result = (T) unitOfWork.registerNewObject(newEntity);
        }
        catch (NoSuchMethodException |
                InstantiationException |
                InvocationTargetException |
                IllegalAccessException e)
        {
            throw new CPEntityException(e);
        }
        return result;
    }

    public static ObjectChangeSet getChangeSet(EntityManager entityManager, CPEntityBase entity)
    {
        ObjectChangeSet result = null;

        UnitOfWorkChangeSet unitOfWorkChangeSet = entityManager.unwrap(UnitOfWork.class).getCurrentChanges();
        if (unitOfWorkChangeSet != null)
        {
            result = unitOfWorkChangeSet.getObjectChangeSetForClone(entity);
        }

        return result;
    }

    public class FactoryToken
    {
        private FactoryToken()
        {

        }
    }
}
