/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.api;

import java.util.Map;

/**
 * Base interface for all entities in the Cloud Platform.
 */
public interface CPEntity
{
    /**
     * Get the DB primary key for the Item.
     * Will not be null.
     * @return Long primary key
     */
    Long getPk();

    /**
     * Get the property from this object with the given JSON name.
     * @param name String JSON Field name
     * @return Object property, null if not found.
     */
    Object getProperty(String name);

    /**
     * Set the property on this object with the given JSON name.
     * The value must match the expected type.
     * @param name String JSON Field name
     * @param value Object property value to set
     */
    void setProperty(String name, Object value);

    /**
     * Push a merge map on the stack.
     *
     * @param map Map of attribute/value
     */
    void pushMergeMap(Map<String, Object> map);

    /**
     * Pop a merge map from the stack.
     * @return Map of attribute/value
     */
    Map<String, Object> popMergeMap();

    /**
     * Get a custom property that has been set
     * @param name String name of the property
     * @param <T> Type of the return object
     * @return Object of the specified type, null if not found
     */
    <T> T getExtended(String name);

    /**
     * Set a custom property.
     * @param name String name of the property
     * @param value Object value to set
     * @return The previous value for that name, may be null
     */
    Object setExtended(String name, Object value);

    /**
     * Return true if all sets of unique keys match.
     * @param entity CPEntity other entity to compare
     * @return true if they match
     */
    boolean isMatch(CPEntity entity);

    /**
     * Return true if all sets of unique keys match.
     * @param inputMap Map of key/value representing the CPEntity
     * @return true if they match
     */
    boolean isMatch(Map<String, Object> inputMap);

}
