/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.manh.cp.fw.entity.domain.api.CPAuditableEntity;
import com.manh.cp.fw.entity.domain.converter.LocalCreatedDateTimeConverter;
import com.manh.cp.fw.entity.domain.converter.LocalUpdatedDateTimeConverter;
import com.manh.cp.fw.entity.support.factory.EntityFactory;

@MappedSuperclass
public abstract class CPAuditableEntityBase extends CPEntityBase implements CPAuditableEntity
{
	@Column(name = "CREATED_BY", nullable = false, length = 100)
    private String createdBy;

    @Column(name = "CREATED_TIMESTAMP", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", nullable = false)
    @Convert(converter = LocalCreatedDateTimeConverter.class)
    private LocalDateTime createdTimestamp;

    @Column(name = "UPDATED_BY", nullable = false, length = 100)
    private String updatedBy;

    @Column(name = "UPDATED_TIMESTAMP", columnDefinition = "TIMESTAMP AS CURRENT_TIMESTAMP", nullable = false)
    @Convert(converter = LocalUpdatedDateTimeConverter.class)
    private LocalDateTime updatedTimestamp;

    @Version
    @Column(name = "VERSION", nullable = false, length = 9)
    private Long version = 0L;

    protected CPAuditableEntityBase()
    {
        super();
    }

    public CPAuditableEntityBase(EntityFactory.FactoryToken token)
    {
        super(token);
    }

    @Override
	public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Override
	public LocalDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(LocalDateTime createdTimestamp)
    {
        this.createdTimestamp = createdTimestamp;
    }

    @Override
	public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Override
	public LocalDateTime getUpdatedTimestamp()
    {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(LocalDateTime updatedTimestamp)
    {
        this.updatedTimestamp = updatedTimestamp;
    }

    protected Long getVersion()
    {
        return this.version;
    }

    protected void setVersion(Long version)
    {
        this.version = version;
    }

    @Override
    public String toString()
    {
        return "CPAuditableEntityBase{" +
                "createdBy='" + createdBy + '\'' +
                ", createdTimestamp=" + createdTimestamp +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedTimestamp=" + updatedTimestamp +
                ", version=" + version +
                '}' +
                super.toString();
    }
}
