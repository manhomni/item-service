/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import com.manh.cp.fw.entity.domain.api.CPEntity;

import java.util.function.BiConsumer;

public class CPFieldOverride
{
    private String jsonName;
    private BiConsumer<CPEntity, Object> marshallOverride;
    private BiConsumer<CPEntity, Object> unMarshallOverride;

    private CPFieldOverride()
    {
        // Must be built using Builder
    }

    private CPFieldOverride(Builder builder)
    {
        this.jsonName = builder.jsonName;
        this.marshallOverride = builder.marshallOverride;
        this.unMarshallOverride = builder.unMarshallOverride;
    }


    public static class Builder
    {
        private final String jsonName;
        private BiConsumer<CPEntity, Object> marshallOverride;
        private BiConsumer<CPEntity, Object> unMarshallOverride;

        public Builder(String jsonName)
        {
            this.jsonName = jsonName;
        }

        public Builder marshallOverride(BiConsumer<CPEntity, Object> marshallOverride)
        {
            this.marshallOverride = marshallOverride;
            return this;
        }

        public Builder unMarshallOverride(BiConsumer<CPEntity, Object> unMarshallOverride)
        {
            this.unMarshallOverride = unMarshallOverride;
            return this;
        }

        public CPFieldOverride build()
        {
            return new CPFieldOverride(this);
        }
    }

    public String getJsonName()
    {
        return jsonName;
    }

    public BiConsumer<CPEntity, Object> getMarshallOverride()
    {
        return marshallOverride;
    }

    public BiConsumer<CPEntity, Object> getUnMarshallOverride()
    {
        return unMarshallOverride;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CPFieldOverride that = (CPFieldOverride) o;

        if (!jsonName.equals(that.jsonName))
            return false;
        if (marshallOverride != null ? !marshallOverride.equals(that.marshallOverride) : that.marshallOverride != null)
            return false;
        if (unMarshallOverride != null ? !unMarshallOverride.equals(that.unMarshallOverride) :
                that.unMarshallOverride != null)
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return jsonName.hashCode();
    }

    @Override public String toString()
    {
        return "CPFieldOverride{" +
                "jsonName='" + jsonName + '\'' +
                ", marshallOverride=" + marshallOverride +
                ", unMarshallOverride=" + unMarshallOverride +
                '}';
    }
}
