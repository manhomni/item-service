/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.api;

import java.time.LocalDateTime;

/**
 * Base Cloud Platform interface for entities with audit fields
 */
public interface CPAuditableEntity extends CPEntity
{
    /**
     * Get the user name who originally created this item.
     * @return String creator
     */
    String getCreatedBy();

    /**
     * Get the time when the item was created.
     * @return LocalDateTime creation time
     */
    LocalDateTime getCreatedTimestamp();

    /**
     * Get the user name who last updated this item.
     * @return String updater
     */
    String getUpdatedBy();

    /**
     * Get the time when the item was last updated.
     * @return LocalDateTime update time
     */
    LocalDateTime getUpdatedTimestamp();

}
