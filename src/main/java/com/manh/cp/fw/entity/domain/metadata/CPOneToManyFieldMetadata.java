/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import javax.persistence.metamodel.Attribute;

public class CPOneToManyFieldMetadata extends CPFieldMetadata
{
    private String targetEntityName;

    CPOneToManyFieldMetadata(Builder builder)
    {
        super(builder.jsonName, builder.defaultTemplate);
        this.targetEntityName = builder.targetEntityName;
    }

    @Override public Attribute.PersistentAttributeType getAttributeType()
    {
        return Attribute.PersistentAttributeType.ONE_TO_MANY;
    }

    public static class Builder
    {
        private final String jsonName;
        private boolean defaultTemplate = true;
        private String targetEntityName;

        public Builder(String jsonName, String targetEntityName)
        {
            this.jsonName = jsonName;
            this.targetEntityName = targetEntityName;
        }

        public Builder defaultTemplate(boolean defaultTemplate)
        {
            this.defaultTemplate = defaultTemplate;
            return this;
        }

        public CPOneToManyFieldMetadata build()
        {
            return new CPOneToManyFieldMetadata(this);
        }
    }

    public String getTargetEntityName()
    {
        return this.targetEntityName;
    }

}
