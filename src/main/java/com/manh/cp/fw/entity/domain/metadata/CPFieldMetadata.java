/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.metadata;

import javax.persistence.metamodel.Attribute;

public abstract class CPFieldMetadata
{
    private String jsonName;
    private boolean defaultTemplate = true;

    protected CPFieldMetadata()
    {
        // Must be built using Builder
    }

    protected CPFieldMetadata(String jsonName, boolean defaultTemplate)
    {
        this.jsonName = jsonName;
        this.defaultTemplate = defaultTemplate;
    }

    public abstract Attribute.PersistentAttributeType getAttributeType();

    public String getJsonName()
    {
        return jsonName;
    }

    public boolean isDefaultTemplate()
    {
        return defaultTemplate;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        CPFieldMetadata that = (CPFieldMetadata) o;

        if (!jsonName.equals(that.jsonName))
            return false;

        if (!defaultTemplate == that.defaultTemplate)
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return jsonName.hashCode();
    }

    @Override public String toString()
    {
        return "CPFieldMetadata{" +
                "jsonName='" + jsonName + '\'' +
                "defaultTemplate='" + defaultTemplate + '\'' +
                '}';
    }
}
