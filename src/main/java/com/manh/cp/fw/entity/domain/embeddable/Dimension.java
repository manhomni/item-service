/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */

package com.manh.cp.fw.entity.domain.embeddable;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Embeddable;

import com.manh.cp.fw.entity.domain.enums.UOM;

@Cacheable
@Embeddable
public class Dimension implements Serializable
{
	private Double height;

    private Double width;

    private Double length;

    private UOM uom;

    protected Dimension()
    {
    }

    public Dimension(long height, long width, long length, UOM uom)
    {
        this.height = (double) height;
        this.width = (double) width;
        this.length = (double) length;
        this.uom = uom;
    }

    public Dimension(double height, double width, double length, UOM uom)
    {
        this.height = height;
        this.width = width;
        this.length = length;
        this.uom = uom;
    }

    public Double getHeight()
    {
        return height;
    }

    public void setHeight(Double height)
    {
        this.height = height;
    }

    public Double getWidth()
    {
        return width;
    }

    public void setWidth(Double width)
    {
        this.width = width;
    }

    public Double getLength()
    {
        return length;
    }

    public void setLength(Double length)
    {
        this.length = length;
    }

    public UOM getUom()
    {
        return uom;
    }

    public void setUom(UOM uom)
    {
        this.uom = uom;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Dimension dimension = (Dimension) o;

        if (!height.equals(dimension.height))
            return false;
        if (!length.equals(dimension.length))
            return false;
        if (uom != dimension.uom)
            return false;
        if (!width.equals(dimension.width))
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = height.hashCode();
        result = 31 * result + width.hashCode();
        result = 31 * result + length.hashCode();
        result = 31 * result + uom.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "Dimension{" +
                "height=" + height +
                ", width=" + width +
                ", length=" + length +
                ", uom=" + uom +
                '}';
    }
}
