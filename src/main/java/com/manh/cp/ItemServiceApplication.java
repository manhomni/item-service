/*
 * Copyright &#169; 2015 Manhattan Associates, Inc.  All Rights Reserved.
 *
 * Confidential, Proprietary and Trade Secrets Notice
 *
 * Use of this software is governed by a license agreement. This software
 * contains confidential, proprietary and trade secret information of
 * Manhattan Associates, Inc. and is protected under United States and
 * international copyright and other intellectual property laws. Use, disclosure,
 * reproduction, modification, distribution, or storage in a retrieval system in
 * any form or by any means is prohibited without the prior express written
 * permission of Manhattan Associates, Inc.
 *
 * Manhattan Associates, Inc.
 * 2300 Windy Ridge Parkway, 10th Floor
 * Atlanta, GA 30339 USA
 */
package com.manh.cp;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;

@SpringBootApplication
@EnableEurekaClient
public class ItemServiceApplication extends JpaBaseConfiguration
{

    public static void main(String[] args)
    {
        SpringApplication.run(ItemServiceApplication.class, args);
    }

    @Override
   	protected AbstractJpaVendorAdapter createJpaVendorAdapter() 
    {
   		EclipseLinkJpaVendorAdapter adapter = new EclipseLinkJpaVendorAdapter();
   		return adapter;
   	}

   	@Override
   	protected Map<String, Object> getVendorProperties() 
   	{
   		HashMap<String, Object> map = new HashMap<String, Object>();
   		return map;
   	}
   	
}